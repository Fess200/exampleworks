//
//  AppDelegate.swift
//  Masters
//
//  Created by Andrew on 05.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        
        configurateApplication()
        
        let window = UIWindow(frame: UIScreen.main.bounds)
        let profileService = assembler.resolver.resolve(ProfileServiceProtocol.self)
        if let _ = profileService?.getUser() {
            window.rootViewController = assembler.resolver.resolve(MenuRouterProtocol.self)?.rootController
        } else {
            window.rootViewController = assembler.resolver.resolve(StartRouterProtocol.self, argument: window)?.rootController
        }
        window.makeKeyAndVisible()
        self.window = window
        
        return true
    }
    
    func configurateApplication() {
        UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName:UIColor.white]
        UINavigationBar.appearance().barTintColor = UIColor.init(red: 96.0/255.0, green: 125.0/255.0, blue: 139.0/255.0, alpha: 1)
        UINavigationBar.appearance().isTranslucent = false
        UINavigationBar.appearance().tintColor = UIColor.white
        
        UIBarButtonItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName:UIColor.white], for: .normal)
        
        assembler.resolver.resolve(AppDataServiceProtocol.self)?.updateVolcabuary(complete: { _ in})
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

