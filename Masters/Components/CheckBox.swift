//
//  CheckButton.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class CheckBox: UIButton {
    
    var isSelect: Bool = false {
        didSet {
            setImage(UIImage(named: isSelect ? "cheсkBoxOn" : "cheсkBox") , for: .normal)
        }
    }

}
