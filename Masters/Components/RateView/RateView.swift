//
//  RateView.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import SnapKit
import RxSwift

class RateView: UIView {
    
    private static let starImage = "star"
    private static let starEmptyImage = "starEmpty"
    
    private var buttons = [UIButton]()
    
    var rate = Variable(0)
    
    @IBInspectable var sizeStar: CGFloat = 40
    @IBInspectable var spaceBetweenStar: CGFloat = 5
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        let countStar = 5
        for i in 0..<countStar {
            let button = UIButton(type: .system)
            button.setImage(UIImage(named: RateView.starEmptyImage)?.withRenderingMode(.alwaysOriginal), for: .normal)
            button.addTarget(self, action: #selector(selectButton(button:)), for: .touchUpInside)
            button.tag = i+1
            
            addSubview(button)
            button.snp.makeConstraints({ (make) in
                make.centerY.equalTo(self.snp.centerY)
                let allWidth = spaceBetweenStar*CGFloat(countStar-1)+CGFloat(countStar)*sizeStar
                make.centerX.equalTo(self.snp.centerX).offset(-allWidth/2+(spaceBetweenStar+sizeStar)*CGFloat(i))
                make.width.equalTo(button.snp.height).multipliedBy(1)
                make.width.equalTo(sizeStar)
            })
            
            buttons.append(button)
        }
        
        selectRate(rate: rate.value)
    }
    
    @objc private func selectButton(button:UIButton) {
        if rate.value != button.tag {
            rate.value = button.tag
            selectRate(rate: rate.value)
        } else {
            rate.value = 0
            selectRate(rate: rate.value)
        }
    }
    
    private func selectRate(rate:Int) {
        for button in buttons {
            button.setImage(UIImage(named:rate >= button.tag ? RateView.starImage : RateView.starEmptyImage)?.withRenderingMode(.alwaysOriginal), for: .normal)
        }
    }

}
