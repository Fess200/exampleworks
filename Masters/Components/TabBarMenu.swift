//
//  TabBarMenu.swift
//  Masters
//
//  Created by Andrew on 25.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class TabBarMenu: UITabBarItem {
    
    @IBInspectable var imageItem: String = "" {
        didSet {
            image = UIImage(named: imageItem)?.withRenderingMode(.alwaysOriginal)
            selectedImage = UIImage(named: "\(imageItem)Select")?.withRenderingMode(.alwaysOriginal)
            setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 146.0/255.0, green: 146.0/255.0, blue: 146.0/255.0, alpha: 1)], for: .normal)
            setTitleTextAttributes([NSForegroundColorAttributeName: UIColor(red: 76.0/255.0, green: 181.0/255.0, blue: 171.0/255.0, alpha: 1)], for: .selected)
        }
    }
}
