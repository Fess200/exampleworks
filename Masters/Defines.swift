//
//  Defines.swift
//  Masters
//
//  Created by Andrew on 05.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

struct Constants {
    static let phoneRegex = "^(?:\\+)(?:7)(?:\\()[0-9]{3}(?:\\))[0-9]{3}(?:-)[0-9]{2}(?:-)[0-9]{2}$"
}
