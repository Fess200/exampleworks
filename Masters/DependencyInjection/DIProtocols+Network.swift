//
//  DIProtocols+Network.swift
//  Masters
//
//  Created by Andrew on 08.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import Foundation

protocol AppDataNetworkProtocol {
    func getVolcabuary(complete: @escaping ((_ volcabuary:Volcabuary?, _ error:Error?)->()))
}

protocol SearchNetworkProtocol {
    func search(token: String, nickname: String, cityId:String, serviceIds: [Int], rating: Int?, radius: CGFloat?, costFrom: Int?, costTo: Int?, complete: @escaping ((_ volcabuary:Volcabuary?, _ error:Error?)->()))
}

protocol ProfileNetworkProtocol {
    func sendCodeSMS(phone:String, complete: @escaping ((_ success:Bool, _ error:NetworkError?)->())) -> URLSessionTask?
    func registerUser(name:String, nick:String, phone:String, address:String, avatar: UIImage?, isMaster: Bool, about:String?, holidays: [DayWeekend]?, breaks: [PeriodInterval]?, examplesWorks: [UIImage]?, complete: @escaping ((_ user:User?, _ error:NetworkError?)->()))
}
