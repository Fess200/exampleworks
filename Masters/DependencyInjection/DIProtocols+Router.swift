//
//  DIProtocols+Router.swift
//  Masters
//
//  Created by Andrew on 13.07.16.
//  Copyright © 2016 Andrew. All rights reserved.
//

import UIKit

protocol RouterDismissProtocol {
    func dismiss()
}

protocol RouterBackProtocol {
    func goBack()
}

protocol StartRouterProtocol: RouterBackProtocol, RouterDismissProtocol {
    var window: UIWindow { get }
    
    var rootController: UIViewController {get}
    
    func showEnterSMS()
    func showRegistration(phone: String)
    func showSelectPhoto(complete:@escaping (_ image:UIImage?)->())
    func showAddService(viewModel:AddServiceViewModel)
    func showSelectSeviceCategory(viewModel:SelectServiceCategoryViewModel)
    func showSelectSevice(viewModel:SelectServiceViewModel)
    func showSelectDuration(viewModel:SelectDurationViewModel)
    func showDetailPreview(viewModel:DetailPreviewExampleWorkViewModel)
    
    func showEditPeriodInterval(viewModel:PeriodIntervalViewModel)
    func showSelectPeriodInterval(viewModel:SelectPeriodIntervalViewModel)
    
    func showEditDaysWeekend(viewModel:WeekendDaysViewModel)
    
    func showSelectData(viewModel:SelectDataViewModel)
    
    func changeToTabBarController()
    
    func showAlertNeedReg(complete:@escaping ((_ success:Bool)->()))
}

// MARK: - Menu

protocol MenuRouterProtocol {
    var rootController: UIViewController {get}
}

// MARK: - MenuPoints

protocol MenuPointRouterProtocol {
    var rootController: UIViewController {get}
}

protocol HomeRouterProtocol: MenuPointRouterProtocol {
}

protocol ScheduleRouterProtocol: MenuPointRouterProtocol {
}

protocol RequestsRouterProtocol: MenuPointRouterProtocol {
}

protocol ProfileRouterProtocol: MenuPointRouterProtocol {
}

protocol SettingsRouterProtocol: MenuPointRouterProtocol {
}

protocol SearchRouterProtocol: MenuPointRouterProtocol, RouterDismissProtocol, RouterBackProtocol {
    func showSelectData(viewModel:SelectDataViewModel)
    func showSearchSelectService(viewModel:SearchSelectServiceViewModel)
}

protocol MastersRouterProtocol: MenuPointRouterProtocol {
}
