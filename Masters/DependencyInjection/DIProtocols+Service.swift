//
//  DIProtocols+Service.swift
//  Masters
//
//  Created by Andrew on 06.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

protocol ProfileServiceProtocol {
    func getUser() -> User?
    func isMaster() -> Bool
    func token() -> String?
    func logout()
    func sendCodeSMS(phone:String, complete: @escaping ((_ success:Bool, _ error:NetworkError?)->())) -> URLSessionTask?
    func registerUser(name:String, nick:String, phone:String, address:String, avatar: UIImage?, isMaster: Bool, about:String?, holidays: [DayWeekend]?, breaks: [PeriodInterval]?, examplesWorks: [UIImage]?, complete: @escaping ((_ success:Bool, _ error:NetworkError?)->()))
}

protocol AppDataServiceProtocol {
    func updateVolcabuary(complete: @escaping ((_ volcabuary: Volcabuary?)->()))
    func getCategories(complete: @escaping ((_ categories: [ServiceCategory]?)->()))
    func getServices(categoryId: Int, complete: @escaping ((_ services: [Service]?)->()))
    func getRegions(complete: @escaping ((_ services: [Region])->()))
}
