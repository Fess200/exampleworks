//
//  ManagerAssembly.swift
//  Masters
//
//  Created by Andrew on 13.07.16.
//  Copyright © 2016 Andrew. All rights reserved.
//

import Swinject

let assembler = try! Assembler(assemblies: [
    ManagerAssembly()
    ])

class ManagerAssembly: Assembly {
    func assemble(container: Container) {
        
        let network = Network()
        
        container.register(SearchNetworkProtocol.self) { _ in
            network
            }.inObjectScope(.container)
        
        container.register(AppDataServiceProtocol.self) { _ in
            AppDataService(network: network)
        }.inObjectScope(.container)
        
        let profileService = ProfileService(network: network)
        
        container.register(ProfileServiceProtocol.self) { _ in  profileService}.inObjectScope(.container)
        
        let menuRouter = MenuRouter(profileService: profileService)
        
        container.register(MenuRouterProtocol.self) { _ in  MenuRouter(profileService: profileService) }.inObjectScope(.container)
        
        container.register(StartRouterProtocol.self) { _, window in
            StartRouter(menuRouter: menuRouter, profileService: profileService, window: window)
        }.inObjectScope(.container)
        
        container.register(HomeRouterProtocol.self) { _ in  HomeRouter()}
        container.register(ScheduleRouterProtocol.self) { _ in  ScheduleRouter()}
        container.register(RequestsRouterProtocol.self) { _ in  RequestsRouter()}
        container.register(ProfileRouterProtocol.self) { _ in  ProfileRouter(profileService: profileService)}
        container.register(SettingsRouterProtocol.self) { _ in  SettingsRouter()}
        container.register(SearchRouterProtocol.self) { _ in  SearchRouter()}
        container.register(MastersRouterProtocol.self) { _ in  MastersRouter()}
    }
}
