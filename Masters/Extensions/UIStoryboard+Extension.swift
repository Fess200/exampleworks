//
//  UIStoryboard+Extension.swift
//  Masters
//
//  Created by Andrew on 16.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

extension UIStoryboard {
    
    class func storyBoardCommon() -> UIStoryboard {
        return UIStoryboard.init(name: "Common", bundle: nil)
    }
    
    class func storyBoardStartScreen() -> UIStoryboard {
        return UIStoryboard.init(name: "StartSreens", bundle: nil)
    }
    
    class func storyBoardAlerts() -> UIStoryboard {
        return UIStoryboard.init(name: "Alerts", bundle: nil)
    }
    
    class func masters() -> UIStoryboard {
        return UIStoryboard(name: "Masters", bundle: nil)
    }
    
    class func profile() -> UIStoryboard {
        return UIStoryboard(name: "Profile", bundle: nil)
    }
    
    class func home() -> UIStoryboard {
        return UIStoryboard(name: "Home", bundle: nil)
    }
    
    class func requests() -> UIStoryboard {
        return UIStoryboard(name: "Requests", bundle: nil)
    }
    
    class func schedule() -> UIStoryboard {
        return UIStoryboard(name: "Schedule", bundle: nil)
    }
    
    class func search() -> UIStoryboard {
        return UIStoryboard(name: "Search", bundle: nil)
    }
    
    class func settings() -> UIStoryboard {
        return UIStoryboard(name: "Settings", bundle: nil)
    }
}
