//
//  Region.swift
//  Masters
//
//  Created by Andrew on 01.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import ObjectMapper

class Region: Mappable {
    
    private static let keyCode = "code"
    
    dynamic var code: String = ""
    dynamic var name: String = ""
    
    required init?(map: Map) {
        guard let _ = map.JSON[Region.keyCode] else {
            return nil
        }
    }
    
    func mapping(map: Map) {
        code <- map[Region.keyCode]
        name <- map["name"]
    }
    
}
