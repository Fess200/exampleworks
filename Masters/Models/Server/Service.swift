//
//  Service.swift
//  Masters
//
//  Created by Andrew on 01.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import RealmSwift
import ObjectMapper

class Service: Object, Mappable {
    
    private static let keyId = "id"
    
    dynamic var id: Int = 0
    dynamic var title: String?
    dynamic var categoryId: Int = 0
    
    func primaryKey() -> String? {
        return Service.keyId
    }
    
    required convenience init?(map: Map) {
        guard let id = map.JSON[Service.keyId] as? String, let _ = Int(id) else {
            return nil
        }
        self.init()
    }
    
    func mapping(map: Map) {
        id <- (map[Service.keyId], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        title <- map["title"]
        categoryId <- (map["category_id"], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
    }
    
    static func ==(lhs: Service, rhs: Service) -> Bool {
        return lhs.id == rhs.id
    }
}
