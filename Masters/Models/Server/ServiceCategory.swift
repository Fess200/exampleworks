//
//  ServiceCategory.swift
//  Masters
//
//  Created by Andrew on 01.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import ObjectMapper

class ServiceCategory: Mappable, Equatable {
    
    private static let keyId = "id"
    
    var id: Int!
    var title: String?
    var services: [Service]?
    
    required init?(map: Map) {
        guard let id = map.JSON[ServiceCategory.keyId] as? String, let _ = Int(id) else {
            return nil
        }
    }
    
    func mapping(map: Map) {
        id <- (map[ServiceCategory.keyId], TransformOf<Int, String>(fromJSON: { Int($0!) }, toJSON: { $0.map { String($0) } }))
        title <- map["title"]
        services <- map["services"]
    }
    
    static func ==(lhs: ServiceCategory, rhs: ServiceCategory) -> Bool {
        return lhs.id == rhs.id
    }
}
