//
//  Volcabuary.swift
//  Masters
//
//  Created by Andrew on 06.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import ObjectMapper

class Volcabuary: Mappable {
    
    var serviceCategories: [ServiceCategory]!
    var regions: [Region]!
//    var answers: [Service]?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        serviceCategories <- map["service_category"]
        regions <- map["regions"]
    }
}
