//
//  User.swift
//  Masters
//
//  Created by Andrew on 12.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import ObjectMapper
import RealmSwift

class User: Object, Mappable {
    dynamic var id:Int = 0
    dynamic var token:String = ""
    dynamic var isMaster: Bool = false
    dynamic var nickname:String = ""
    dynamic var username:String = ""
    dynamic var address:String = ""
    dynamic var phone:String = ""
    
    required convenience init?(map: Map) {
        self.init()
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        isMaster <- map["is_master"]
        token <- map["auth_key"]
        nickname <- map["nickname"]
        username <- map["username"]
        address <- map["address"]
        phone <- map["phone"]
    }
}
