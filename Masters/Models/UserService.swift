//
//  UserService.swift
//  Masters
//
//  Created by Andrew on 01.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import RealmSwift

class MasterService: Object {
    dynamic var cost: Double = 0
    dynamic var duration: Int = 0
    dynamic var service: Service!
    
    static func ==(lhs: MasterService, rhs: MasterService) -> Bool {
        return lhs.service == rhs.service
    }
}
