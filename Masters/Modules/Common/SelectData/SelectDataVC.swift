//
//  SelectDataVC.swift
//  StaffFloor
//
//  Created by Andrew on 05.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class SelectDataVC: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var pickerView: UIPickerView!
    
    private static let cellIdentifier = "SelectDataCell"
    
    var viewModel: SelectDataViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        pickerView.selectRow(viewModel.indexSelect, inComponent: 0, animated: false)
    }
    
    @IBAction func onReady(_ sender: Any) {
        viewModel.select(index: pickerView.selectedRow(inComponent: 0))
    }

    @IBAction func onClose(_ sender: Any) {
        viewModel.close()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.countData
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.title(index: row)
    }
    
}
