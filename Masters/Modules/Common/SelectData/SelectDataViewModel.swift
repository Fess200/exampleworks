//
//  SelectDataViewModel.swift
//  StaffFloor
//
//  Created by Andrew on 05.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SelectDataViewModel: NSObject {
    
    private let router: RouterDismissProtocol
    private let values: [Any]
    let indexSelect: Int
    private let convertBlock: ((_ object:Any)->String)
    
    let selectedVariable = PublishSubject<Any>()
    
    let disposeBag = DisposeBag()
    
    var countData: Int {
        return values.count
    }
    
    init(router: RouterDismissProtocol, values: [Any], indexSelect: Int, convertBlock: @escaping ((_ object:Any)->String)) {
        self.router = router
        self.values = values
        self.indexSelect = indexSelect
        self.convertBlock = convertBlock
    }
    
    func select(index:Int) {
        selectedVariable.onNext(values[index])
        router.dismiss()
    }
    
    func close() {
        router.dismiss()
    }
    
    func title(index:Int) -> String {
        return convertBlock(values[index])
    }
    
}
