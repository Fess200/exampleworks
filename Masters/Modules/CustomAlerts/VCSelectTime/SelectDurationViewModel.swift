//
//  SelectDurationViewModel.swift
//  Masters
//
//  Created by Andrew on 11.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SelectDurationViewModel: NSObject, SelectTimeViewModelProtocol {
    
    private let router:RouterDismissProtocol
    
    let hours:[String]
    let minutes:[String]
    
    let duration: Variable<Int> = Variable(0)
    
    init(router:RouterDismissProtocol) {
        self.router = router
        
        var hours = [String]()
        for i in 0..<24 {
            hours.append("\(i)")
        }
        self.hours = hours
        
        var minutes = [String]()
        for i in 0..<60 {
            minutes.append("\(i)")
        }
        self.minutes = minutes
    }
    
    func select(indexHour:Int, indexMinute:Int) {
        
        if let hours = Int(self.hours[indexHour]), let minutes = Int(self.minutes[indexMinute]) {
            duration.value = hours*60+minutes
        }
        
        router.dismiss()
    }
    
    func dismiss() {
        router.dismiss()
    }
}
