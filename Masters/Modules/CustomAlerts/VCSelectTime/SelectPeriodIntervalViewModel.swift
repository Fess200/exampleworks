//
//  SelectPeriodIntervalViewModel.swift
//  Masters
//
//  Created by Andrew on 02.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SelectPeriodIntervalViewModel: NSObject, SelectTimeViewModelProtocol {
    
    private let router:RouterDismissProtocol
    
    let hours:[String]
    let minutes:[String]
    
    var periodInterval: Variable<PeriodDateInterval>
    
    init(router:RouterDismissProtocol, periodInterval: PeriodDateInterval?) {
        self.router = router
        
        if periodInterval == nil {
            self.periodInterval = Variable(PeriodDateInterval(hours: 0, minutes: 0))
        } else {
            self.periodInterval = Variable(periodInterval!)
        }
        
        var hours = [String]()
        for i in 0..<24 {
            hours.append("\(i)")
        }
        self.hours = hours
        
        var minutes = [String]()
        for i in 0..<60 {
            minutes.append("\(i)")
        }
        self.minutes = minutes
    }
    
    func select(indexHour:Int, indexMinute:Int) {
        
        if let hours = Int(self.hours[indexHour]), let minutes = Int(self.minutes[indexMinute]) {
            periodInterval.value = PeriodDateInterval(hours: hours, minutes: minutes)
        }
        
        router.dismiss()
    }
    
    func dismiss() {
        router.dismiss()
    }
}
