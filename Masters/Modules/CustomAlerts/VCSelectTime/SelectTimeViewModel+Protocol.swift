//
//  SelectTimeViewModel+Protocol.swift
//  Masters
//
//  Created by Andrew on 02.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import Foundation

protocol SelectTimeViewModelProtocol {
    var hours:[String] {get}
    var minutes:[String] {get}
    
    func select(indexHour:Int, indexMinute:Int)
    func dismiss()
}
