//
//  VCSelectTime.swift
//  Masters
//
//  Created by Andrew on 11.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class VCSelectTime: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {

    @IBOutlet weak var datePicker: UIPickerView!
    @IBOutlet weak var viewContent: UIView!
    var viewModel:SelectTimeViewModelProtocol!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewContent.layer.cornerRadius = 15
        viewContent.layer.masksToBounds = true
    }
    
    // MARK: - UIPickerViewDataSource
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    public func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return viewModel.hours.count
        } else {
            return viewModel.minutes.count
        }
    }
    
    // MARK: - UIPickerViewDelegate
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0 {
            return viewModel.hours[row]
        } else {
            return viewModel.minutes[row]
        }
    }
    
    // MARK: - Actions
    
    @IBAction func onDismiss(_ sender: Any) {
        viewModel.dismiss()
    }
    
    @IBAction func onOk(_ sender: Any) {
        viewModel.select(indexHour: datePicker.selectedRow(inComponent: 0) , indexMinute: datePicker.selectedRow(inComponent: 1))
    }
}
