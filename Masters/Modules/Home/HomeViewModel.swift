//
//  HomeViewModel.swift
//  Masters
//
//  Created by Andrew on 25.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class HomeViewModel: NSObject {
    
    struct SectionHome {
        let title: String
        let viewModels: [HomeCellViewModel]
    }
    
    private let router:HomeRouterProtocol
    
    let isMaster:Bool
    
    var sections = Variable([SectionHome]())
    
    init(router:HomeRouterProtocol) {
        self.router = router
        self.isMaster = assembler.resolver.resolve(ProfileServiceProtocol.self)!.isMaster()
    }
    
    func requestModel() {
        
    }
}
