//
//  VCHome.swift
//  Masters
//
//  Created by Andrew on 16.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class VCHome: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var viewNoFound: UIView!
    @IBOutlet weak var labelNoData: UILabel!
    @IBOutlet weak var tableViewData: UITableView!
    @IBOutlet weak var indicatorLoad: UIActivityIndicatorView!
    
    var viewModel: HomeViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        labelNoData.text = viewModel.isMaster ? "Записей нет. Найти мастера." : "Записей нет"
        
        viewModel.requestModel()
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.sections.value.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.sections.value[section].viewModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeCell", for: indexPath) as! HomeCell
        cell.viewModel = viewModel.sections.value[indexPath.section].viewModels[indexPath.row]
        return cell
    }

}
