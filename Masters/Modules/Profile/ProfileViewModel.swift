//
//  ProfileViewModel.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class ProfileViewModel: NSObject {
    let address: Variable<String>
    let phone: Variable<String>
    let avatar: Variable<UIImage?>
    let nickname: Variable<String>
    let name: Variable<String>
    
    override init() {
        let user = assembler.resolver.resolve(ProfileServiceProtocol.self)!.getUser()!
        address = Variable(user.address)
        phone = Variable(user.phone)
        avatar = Variable(nil)
        nickname = Variable(user.nickname)
        name = Variable(user.username)
        super.init()
    }
}
