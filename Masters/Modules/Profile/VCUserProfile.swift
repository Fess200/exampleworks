//
//  VCUserProfile.swift
//  Masters
//
//  Created by Andrew on 16.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VCUserProfile: UITableViewController {

    @IBOutlet weak var imageViewAvatar: UIImageView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelNickName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var labelPhone: UILabel!
    
    private static let rowAddressIndex = 1
    
    private let disposeBag = DisposeBag()
    
    var viewModel: ProfileViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.name.asDriver().drive(labelName.rx.text).addDisposableTo(disposeBag)
        viewModel.nickname.asDriver().map{ "@\($0)" }.drive(labelNickName.rx.text).addDisposableTo(disposeBag)
        viewModel.address.asDriver().drive(labelAddress.rx.text).addDisposableTo(disposeBag)
        viewModel.phone.asDriver().drive(labelPhone.rx.text).addDisposableTo(disposeBag)
        viewModel.avatar.asDriver().drive(onNext: { [weak self] (image) in
            self?.imageViewAvatar.image = image ?? UIImage(named: "profile")
        }).addDisposableTo(disposeBag)
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == VCUserProfile.rowAddressIndex {
            return UITableViewAutomaticDimension
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == VCUserProfile.rowAddressIndex {
            return UITableViewAutomaticDimension
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
}
