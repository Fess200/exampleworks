//
//  SearchCostCell.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchCostCell: UITableViewCell {

    @IBOutlet weak var sliderCost: MARKRangeSlider!
    @IBOutlet weak var labelStart: UILabel!
    @IBOutlet weak var labelEnd: UILabel!
    
    private var disposeBag: DisposeBag!
    
    override func awakeFromNib() {
        
        sliderCost.minimumDistance = 0
        
        super.awakeFromNib()
    }
    
    var viewModel: SearchCostViewModel! {
        willSet {
            disposeBag = DisposeBag()
        }
        didSet {
            viewModel.minCost.asDriver().map({"\($0)"}).drive(labelStart.rx.text).addDisposableTo(disposeBag)
            viewModel.maxCost.asDriver().map({"\($0)"}).drive(labelEnd.rx.text).addDisposableTo(disposeBag)
        }
    }
    
    @IBAction func sliderValueChanged(_ sender: Any) {
        viewModel.setNewMinCost(procent: sliderCost.leftValue)
        viewModel.setNewMaxCost(procent: sliderCost.rightValue)
    }

}
