//
//  SearchCostViewModel.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SearchCostViewModel: NSObject {
    
    private static let maxCostValue: CGFloat = 100
    
    var minCost: Variable<Int> = Variable(0)
    var maxCost: Variable<Int> = Variable(Int(SearchCostViewModel.maxCostValue))
    
    func setNewMinCost(procent:CGFloat) {
        minCost.value = Int(procent*SearchCostViewModel.maxCostValue)
    }
    
    func setNewMaxCost(procent:CGFloat) {
        maxCost.value = Int(procent*SearchCostViewModel.maxCostValue)
    }
}
