//
//  SearchNearCell.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchNearCell: UITableViewCell {

    @IBOutlet weak var sliderNear: UISlider!
    @IBOutlet weak var labelNear: UILabel!
    
    private var disposeBag: DisposeBag!

    var viewModel: SearchNearViewModel! {
        willSet {
            disposeBag = DisposeBag()
        }
        didSet {
            sliderNear.value = Float(viewModel.distance.value)
            viewModel.distance.asDriver().drive(onNext: { [weak self] (distance) in
                self?.labelNear.text = "в радиусе \(String(format: "%.1f", distance)) км"
            }).addDisposableTo(disposeBag)
        }
    }

    @IBAction func sliderValueChanged(_ sender: Any) {
        viewModel.setNear(procent: CGFloat(sliderNear.value))
    }
}
