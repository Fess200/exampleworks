//
//  SearchNearViewModel.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SearchNearViewModel: NSObject {
    
    private static let maxCostValue: CGFloat = 100
    
    var distance: Variable<CGFloat> = Variable(0)
    
    func setNear(procent:CGFloat) {
        distance.value = procent*SearchNearViewModel.maxCostValue
    }
}
