//
//  SearchRateCell.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchRateCell: UITableViewCell {

    @IBOutlet weak var viewRate: RateView!

    private var disposeBag: DisposeBag!
    
    var viewModel: SearchRateViewModel! {
        willSet {
            disposeBag = DisposeBag()
        }
        didSet {
            viewRate.rate = viewModel.rate
            viewRate.rate.asDriver().skip(1).drive(viewModel.rate).addDisposableTo(disposeBag)
        }
    }

}
