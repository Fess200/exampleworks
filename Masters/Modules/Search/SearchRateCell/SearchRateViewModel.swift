//
//  SearchRateViewModel.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SearchRateViewModel: NSObject {
    var rate: Variable<Int> = Variable(0)
}
