//
//  SearchViewModel.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SearchViewModel: NSObject {
    
    private let router:SearchRouterProtocol
    private let appDataService: AppDataServiceProtocol
    private let profileService: ProfileServiceProtocol
    private let network: SearchNetworkProtocol
    private var region: Region? {
        didSet {
            cityName.value = region?.name
        }
    }
    private var services = [Service]()  {
        didSet {
            var names = ""
            for i in 0..<services.count {
                let service = services[i]
                if let title = service.title {
                    names.append(title)
                    if i != services.count-1 {
                        names.append(", ")
                    }
                }
            }
            servicesName.value = names.characters.count > 0 ? names : nil
        }
    }
    
    let viewModelSearchCost: SearchCostViewModel = SearchCostViewModel()
    let viewModelSearchRate: SearchRateViewModel = SearchRateViewModel()
    let viewModelSearchNear: SearchNearViewModel = SearchNearViewModel()
    
    var cityName: Variable<String?> = Variable(nil)
    var servicesName: Variable<String?> = Variable(nil)
    var nickname: Variable<String?> = Variable(nil)
    
    var isCheckMore = Variable(false)
    var isCheckCost = Variable(false)
    var isCheckRate = Variable(false)
    var isCheckNear = Variable(false)
    
    init(router:SearchRouterProtocol) {
        
        self.router = router
        
        appDataService = assembler.resolver.resolve(AppDataServiceProtocol.self)!
        profileService = assembler.resolver.resolve(ProfileServiceProtocol.self)!
        network = assembler.resolver.resolve(SearchNetworkProtocol.self)!
        
        super.init()
        
        appDataService.getRegions { [weak self] (regions) in
            if let region = regions.first {
                self?.region = region
            }
        }
    }
    
    func selectAddress() {
        appDataService.getRegions { [weak self] (regions) in
            if let currentViewModel = self {
                var indexSelect = 0
                if let region = currentViewModel.region {
                    if let index = regions.index(where: { $0.code == region.code }) {
                        indexSelect = index
                    }
                }
                let viewModel = SelectDataViewModel(router: currentViewModel.router, values: regions, indexSelect:indexSelect, convertBlock: { (region) -> String in
                    return (region as! Region).name
                })

                viewModel.selectedVariable.asObserver().subscribe(onNext: { (region) in
                    if let region = region as? Region {
                        currentViewModel.region = region
                    }
                }).addDisposableTo(viewModel.disposeBag)
                currentViewModel.router.showSelectData(viewModel: viewModel)
            }
        }
    }
    
    func selectService() {
        appDataService.getCategories { [weak self] (serviceCategories:[ServiceCategory]?) in
            if let currentViewModel = self, let serviceCategories = serviceCategories, let router = self?.router {
                let viewModel = SearchSelectServiceViewModel(router: router, serviceCategories: serviceCategories, services: currentViewModel.services)
                viewModel.services.asDriver().skip(1).drive(onNext: { [weak self] (services) in
                    self?.services = services
                }).addDisposableTo(viewModel.disposeBag)
                currentViewModel.router.showSearchSelectService(viewModel: viewModel)
            }
        }
    }
    
    func search() {
        if let token = profileService.token(), let nickname = nickname.value, let region = region {
            let serviceIds = services.map({ $0.id })
            network.search(token: token, nickname: nickname, cityId: region.code, serviceIds: serviceIds, rating: isCheckRate.value ? viewModelSearchRate.rate.value : nil, radius: isCheckNear.value ? viewModelSearchNear.distance.value : nil, costFrom: isCheckCost.value ? viewModelSearchCost.minCost.value : nil, costTo: isCheckCost.value ? viewModelSearchCost.maxCost.value : nil, complete: { (_, _) in
                
            })
        }
    }
    
}
