//
//  VCSearch.swift
//  Masters
//
//  Created by Andrew on 16.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class VCSearch: UITableViewController {

    @IBOutlet weak var labelServices: UILabel!
    @IBOutlet weak var labelCity: UILabel!
    @IBOutlet weak var textfieldSearch: UITextField!
    @IBOutlet weak var checkBoxMore: CheckBox!
    @IBOutlet weak var checkBoxCost: CheckBox!
    @IBOutlet weak var checkBoxRate: CheckBox!
    @IBOutlet weak var checkBoxNear: CheckBox!
    @IBOutlet weak var buttonSearch: UIButton!
    
    private static let sectionMainIndex = 0
    private static let rowServiceIndex = 1
    private static let rowCityIndex = 2
    
    private static let sectionCostIndex = 1
    private static let rowSliderCostIndex = 1
    
    private static let sectionRateIndex = 2
    private static let rowRateIndex = 1
    
    private static let sectionNearIndex = 3
    private static let rowSliderNearIndex = 1
    
    private static let sectionSearchIndex = 4
    
    private let SearchCostCellIdentifier = "SearchCostCell"
    
    private var disposeBag = DisposeBag()
        
    var viewModel: SearchViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        buttonSearch.layer.cornerRadius = 4
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        viewModel.isCheckMore.asDriver().drive(onNext: { [weak self] (isMore) in
            self?.checkBoxMore.isSelect = isMore
            self?.tableView.reloadData()
        }).addDisposableTo(disposeBag)
        
        viewModel.isCheckCost.asDriver().drive(onNext: { [weak self] (isCost) in
            self?.checkBoxCost.isSelect = isCost
            if let isMore = self?.viewModel.isCheckMore.value, isMore {
                self?.tableView.reloadSections(IndexSet.init(integer: VCSearch.sectionCostIndex) , with: .none)
            }
        }).addDisposableTo(disposeBag)
        
        viewModel.isCheckRate.asDriver().drive(onNext: { [weak self] (isRate) in
            self?.checkBoxRate.isSelect = isRate
            if let isMore = self?.viewModel.isCheckMore.value, isMore {
                self?.tableView.reloadSections(IndexSet.init(integer: VCSearch.sectionRateIndex) , with: .none)
            }
        }).addDisposableTo(disposeBag)
        
        viewModel.isCheckNear.asDriver().drive(onNext: { [weak self] (isNear) in
            self?.checkBoxNear.isSelect = isNear
            if let isMore = self?.viewModel.isCheckMore.value, isMore {
                self?.tableView.reloadSections(IndexSet.init(integer: VCSearch.sectionNearIndex) , with: .none)
            }
        }).addDisposableTo(disposeBag)
        
        viewModel.cityName.asDriver().drive(labelCity.rx.text).addDisposableTo(disposeBag)
        viewModel.servicesName.asDriver().drive(onNext: { [weak self] (servicesName) in
            self?.labelServices.text = servicesName ?? "Выбор услуги"
        }).addDisposableTo(disposeBag)
        
        textfieldSearch.rx.text.asDriver().drive(viewModel.nickname).addDisposableTo(disposeBag)
    }
    
    @IBAction func onCheckMore(_ sender: Any) {
        viewModel.isCheckMore.value = !viewModel.isCheckMore.value
    }
    
    @IBAction func onCheckCost(_ sender: Any) {
        viewModel.isCheckCost.value = !viewModel.isCheckCost.value
    }
    
    @IBAction func onCheckRate(_ sender: Any) {
        viewModel.isCheckRate.value = !viewModel.isCheckRate.value
    }
    
    @IBAction func onCheckNear(_ sender: Any) {
        viewModel.isCheckNear.value = !viewModel.isCheckNear.value
    }
    
    @IBAction func onSearch(_ sender: Any) {
        viewModel.search()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 5
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.isCheckMore.value {
            if section == VCSearch.sectionCostIndex {
                return viewModel.isCheckCost.value ? 2 : 1
            }
            if section == VCSearch.sectionRateIndex {
                return viewModel.isCheckRate.value ? 2 : 1
            }
            if section == VCSearch.sectionNearIndex {
                return viewModel.isCheckNear.value ? 2 : 1
            }
            return super.tableView(tableView, numberOfRowsInSection: section)
        } else {
            if section == VCSearch.sectionCostIndex {
                return 0
            }
            if section == VCSearch.sectionRateIndex {
                return 0
            }
            if section == VCSearch.sectionNearIndex {
                return 0
            }
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        let cell = super.tableView(tableView, cellForRowAt: indexPath)
        if indexPath.section == VCSearch.sectionCostIndex && indexPath.row == VCSearch.rowSliderCostIndex {
            (cell as! SearchCostCell).viewModel = viewModel.viewModelSearchCost
        }
        if indexPath.section == VCSearch.sectionRateIndex && indexPath.row == VCSearch.rowRateIndex {
            (cell as! SearchRateCell).viewModel = viewModel.viewModelSearchRate
        }
        if indexPath.section == VCSearch.sectionNearIndex && indexPath.row == VCSearch.rowSliderNearIndex {
            (cell as! SearchNearCell).viewModel = viewModel.viewModelSearchNear
        }
        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0.001
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == VCSearch.sectionMainIndex {
            return indexPath.row == VCSearch.rowServiceIndex || indexPath.row == VCSearch.rowCityIndex
        }
        
        return false
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == VCSearch.sectionMainIndex {
            if indexPath.row == VCSearch.rowServiceIndex {
                viewModel.selectService()
            }
            if indexPath.row == VCSearch.rowCityIndex {
                viewModel.selectAddress()
            }
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
