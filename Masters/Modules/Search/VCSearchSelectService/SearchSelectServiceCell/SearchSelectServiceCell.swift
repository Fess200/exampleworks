//
//  SearchSelectServiceCell.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SearchSelectServiceCell: UITableViewCell {

    @IBOutlet weak var labelNameService: UILabel!
    @IBOutlet weak var imageViewCheck: UIImageView!
    
    private var disposeBag: DisposeBag!
    
    var viewModel: SearchSelectServiceCellViewModel! {
        willSet {
            disposeBag = DisposeBag()
        }
        didSet {
            labelNameService.text = viewModel.name
            viewModel.select.asDriver().drive(onNext: { [weak self] (select) in
                self?.imageViewCheck.isHidden = !select
            }).addDisposableTo(disposeBag)
        }
    }

}
