//
//  SearchSelectServiceCellViewModel.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SearchSelectServiceCellViewModel: NSObject {
    
    let service:Service
    
    var name: String?
    var select = Variable(false)
    
    init(service:Service, select: Bool) {
        self.service = service
        self.name = service.title
        self.select = Variable(select)
        super.init()
    }
    
}
