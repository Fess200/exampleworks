//
//  SearchSelectServiceHeader.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class SearchSelectServiceHeader: UIView {

    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewArrow: UIImageView!
    
    private var disposeBag: DisposeBag!
    
    var viewModel: SearchSelectServiceHeaderViewModel! {
        willSet {
            disposeBag = DisposeBag()
        }
        didSet {
            labelTitle.text = viewModel.name
            
            viewModel.open.asDriver().drive(onNext: { [weak self] (open) in
                self?.imageViewArrow.image = UIImage(named: open ? "tableAccessoryUp" : "tableAccessoryDown")
            }).addDisposableTo(disposeBag)
        }
    }

    @IBAction func onOpen(_ sender: Any) {
        viewModel.open.value = !viewModel.open.value
    }
}
