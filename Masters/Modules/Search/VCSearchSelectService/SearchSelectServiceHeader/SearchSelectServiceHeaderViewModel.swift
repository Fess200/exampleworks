//
//  SearchSelectServiceHeaderViewModel.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SearchSelectServiceHeaderViewModel: NSObject {
    
    private(set) var name: String?
    var open = Variable(false)
    
    let cellViewModels:[SearchSelectServiceCellViewModel]
    
    init(serviceCategory:ServiceCategory, services:[Service]) {
        name = serviceCategory.title
        
        var cellViewModels = [SearchSelectServiceCellViewModel]()
        if let serviceCategoryServices = serviceCategory.services {
            for service in serviceCategoryServices {
                cellViewModels.append(SearchSelectServiceCellViewModel(service: service, select: services.contains(service)))
            }
        }
        self.cellViewModels = cellViewModels
        
        super.init()
    }
    
}
