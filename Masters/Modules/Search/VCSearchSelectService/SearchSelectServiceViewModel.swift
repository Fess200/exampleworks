//
//  SearchSelectServiceViewModel.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SearchSelectServiceViewModel: NSObject {
    
    private(set) var searchSelectServiceHeaderViewModels = [SearchSelectServiceHeaderViewModel]()
    private let router:SearchRouterProtocol
    
    let disposeBag = DisposeBag()
    
    let services: Variable<[Service]>
    
    init(router:SearchRouterProtocol, serviceCategories:[ServiceCategory], services:[Service]) {
        self.router = router
        self.services = Variable(services)

        for serviceCategory in serviceCategories {
            let viewModel = SearchSelectServiceHeaderViewModel(serviceCategory: serviceCategory, services: services)
            searchSelectServiceHeaderViewModels.append(viewModel)
        }
        
        super.init()
    }
    
    func saveSelectServices() {
        var services = [Service]()
        for searchSelectServiceHeaderViewModel in searchSelectServiceHeaderViewModels {
            for cellViewModel in searchSelectServiceHeaderViewModel.cellViewModels {
                if cellViewModel.select.value {
                    services.append(cellViewModel.service)
                }
            }
        }
        self.services.value = services
        self.router.goBack()
    }
    
}
