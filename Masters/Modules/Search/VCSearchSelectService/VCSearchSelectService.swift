//
//  VCSearchSelectService.swift
//  Masters
//
//  Created by Andrew on 26.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VCSearchSelectService: UITableViewController {

    private static let cellIdentifier = "SearchSelectServiceCell"
    private static let heightHeader: CGFloat = 45
    
    private let disposeBag = DisposeBag()
    
    var viewModel: SearchSelectServiceViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for i in 0..<viewModel.searchSelectServiceHeaderViewModels.count {
            let searchSelectServiceHeaderViewModel = viewModel.searchSelectServiceHeaderViewModels[i]
            searchSelectServiceHeaderViewModel.open.asDriver().skip(1).drive(onNext: { [weak self] (_) in
                self?.tableView.reloadSections(IndexSet(integer: i), with: .none)
            }).addDisposableTo(disposeBag)
        }
    }
    
    @IBAction func onReady(_ sender: Any) {
        viewModel.saveSelectServices()
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return VCSearchSelectService.heightHeader
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UINib(nibName: "SearchSelectServiceHeader", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! SearchSelectServiceHeader
        view.viewModel = viewModel.searchSelectServiceHeaderViewModels[section]
        return view
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.searchSelectServiceHeaderViewModels.count
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.searchSelectServiceHeaderViewModels[section].open.value {
            return viewModel.searchSelectServiceHeaderViewModels[section].cellViewModels.count
        } else {
            return 0
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VCSearchSelectService.cellIdentifier, for: indexPath) as! SearchSelectServiceCell
        cell.viewModel = viewModel.searchSelectServiceHeaderViewModels[indexPath.section].cellViewModels[indexPath.row]
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let select = viewModel.searchSelectServiceHeaderViewModels[indexPath.section].cellViewModels[indexPath.row].select
        select.value = !select.value
    }

}
