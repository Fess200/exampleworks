//
//  LoginPresenter.swift
//  Masters
//
//  Created by Andrew on 08.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class LoginViewModel: NSObject {
    
    private let router:StartRouterProtocol
    private var taskLogin:URLSessionTask?
    
    init(router:StartRouterProtocol) {
        self.router = router
    }
    
    func send(phone:String, complete:@escaping ()->()) {
        taskLogin?.cancel()
        taskLogin = assembler.resolver.resolve(ProfileServiceProtocol.self)?.sendCodeSMS(phone: correct(phone: phone), complete: { (success:Bool, error:NetworkError?) in
            
            complete()
    
            if success {
                self.router.showEnterSMS()
            } else if let code = error?.code, code == 2 {
                self.router.showAlertNeedReg(complete: { (success) in
                    if success {
                        self.router.showRegistration(phone: phone)
                    }
                })
            }
        })
    }
}
