//
//  VCLogin.swift
//  Masters
//
//  Created by Andrew on 05.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class VCLogin: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var textFieldPhone: WTReTextField!
    @IBOutlet weak var buttonEnter: UIButton!
    @IBOutlet weak var layoutBottomLogoWelcome: NSLayoutConstraint!
    @IBOutlet weak var layoutBottomButtonEnter: NSLayoutConstraint!
    
    private var spaceBottomLogoWelcome: CGFloat = 0
    private var spaceBottomButtonEnter: CGFloat = 0
    
    var viewModel:LoginViewModel! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()

        spaceBottomLogoWelcome = layoutBottomLogoWelcome.constant
        spaceBottomButtonEnter = layoutBottomButtonEnter.constant
        
        buttonEnter.layer.cornerRadius = 4.0
        
        textFieldPhone.pattern = Constants.phoneRegex
        textFieldPhone.attributedPlaceholder = NSAttributedString.init(string: "Телефон", attributes: [NSForegroundColorAttributeName: UIColor.white])
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        
        NotificationCenter.default.addObserver(self, selector: #selector(willKeyBoardShow(notification:)), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(willKeyBoardHide(notification:)), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    // MARK: Actions
    
    @IBAction func onEnter(_ sender: Any) {
        guard let phone = textFieldPhone.text else {
            return
        }
        
        buttonEnter.isEnabled = false
        viewModel.send(phone: phone) { 
            self.buttonEnter.isEnabled = true
        }
    }
    
    // MARK: Notifications

    func willKeyBoardShow(notification:Notification) {
        if let userInfo = notification.userInfo {
            let endFrame = (userInfo[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurve = animationFunc(userInfo: userInfo)
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
                let sizeKeyboard = endFrame?.size.height ?? 0
                self.layoutBottomLogoWelcome.constant = self.spaceBottomLogoWelcome + sizeKeyboard
                self.layoutBottomButtonEnter.constant = self.spaceBottomButtonEnter + sizeKeyboard
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    func willKeyBoardHide(notification:Notification) {
        if let userInfo = notification.userInfo {
            let duration:TimeInterval = (userInfo[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0
            let animationCurve = animationFunc(userInfo: userInfo)
            
            UIView.animate(withDuration: duration, delay: TimeInterval(0), options: animationCurve, animations: {
                self.layoutBottomLogoWelcome.constant = self.spaceBottomLogoWelcome
                self.layoutBottomButtonEnter.constant = self.spaceBottomButtonEnter
                self.view.layoutIfNeeded()
            }, completion: nil)
        }
    }
    
    private func animationFunc(userInfo: [AnyHashable : Any]) -> UIViewAnimationOptions {
        let animationCurveRawNSN = userInfo[UIKeyboardAnimationCurveUserInfoKey] as? NSNumber
        let animationCurveRaw = animationCurveRawNSN?.uintValue ?? UIViewAnimationOptions.curveEaseInOut.rawValue
        return UIViewAnimationOptions(rawValue: animationCurveRaw)
    }
    
    // MARK: UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
