//
//  SendSMSPresenter.swift
//  Masters
//
//  Created by Andrew on 08.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

protocol SendSMSPresenterDelegate: NSObjectProtocol {
    func finishSend(success:Bool)
}

class SendSMSPresenter: NSObject {
    
    private weak var view:SendSMSPresenterDelegate!
    private let router:StartRouterProtocol
    
    init(view:SendSMSPresenterDelegate, router:StartRouterProtocol) {
        self.view = view
        self.router = router
    }
    
    func send(code:String) {
    }
    
}
