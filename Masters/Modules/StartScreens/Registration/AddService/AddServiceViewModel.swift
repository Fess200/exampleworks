//
//  AddServiceViewModel.swift
//  Masters
//
//  Created by Andrew on 01.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RealmSwift
import RxSwift

protocol AddServiceViewModelDelegate: NSObjectProtocol {
    func add(service:MasterService)
    func edit(service:MasterService)
    func remove(service:MasterService)
}

class AddServiceViewModel: NSObject {
    
    private let router:StartRouterProtocol
    private var service:MasterService?
    private var appDataService: AppDataServiceProtocol!
    private let disposeBag = DisposeBag()
    
    private var categoryViewModel:SelectServiceCategoryViewModel?
    private var serviceViewModel:SelectServiceViewModel?
    private var selectDurationViewModel:SelectDurationViewModel?
    
    private let existServices:[Service]
    
    weak var delegate: AddServiceViewModelDelegate?
    
    var isAddService: Bool {
        return service == nil
    }
    
    var categoryTitle: Variable<String?> = Variable(nil)
    var serviceTitle: Variable<String?> = Variable(nil)
    var cost: Variable<Double?> = Variable(nil)
    var duration: Variable<String?> = Variable(nil)
    
    init(router:StartRouterProtocol, service:MasterService?, existServices:[Service]) {
        self.router = router
        self.existServices = existServices
        
        appDataService = assembler.resolver.resolve(AppDataServiceProtocol.self)
        
        super.init()
        
        if let service = service {
            self.service = service
            
            appDataService.getCategories(complete: { [weak self] (categories:[ServiceCategory]?) in

                let category = categories?.filter { $0.id == service.service.categoryId }.first
                
                self?.createCategoryViewModel()
                self?.categoryViewModel?.selectCategory.value = category
                self?.createServiceViewModel()
                self?.serviceViewModel?.selectService.value = service.service
            })
            
            createDurationViewModel()
            selectDurationViewModel?.duration.value = service.duration
            cost.value = service.cost
        }
    }
    
    private func createCategoryViewModel() {
        self.categoryViewModel = SelectServiceCategoryViewModel(router: router)
        self.categoryViewModel!.selectCategory.asDriver().distinctUntilChanged({ (category1:ServiceCategory?, category2:ServiceCategory?) -> Bool in
            return category1 == category2
        }).map({ $0?.title }).drive(onNext: { [weak self] (title:String?) in
            self?.categoryTitle.value = title
            self?.serviceViewModel?.resetData()
        }).addDisposableTo(disposeBag)
    }
    
    private func createServiceViewModel() {
        
        let services = existServices.filter({ [weak self] (service:Service) -> Bool in
            if let serviceMaster = self?.service {
                return serviceMaster.service != service
            } else {
                return true
            }
        })
        
        self.serviceViewModel = SelectServiceViewModel(categoryViewModel: categoryViewModel!, router: router, ignoreServices: services)
        self.serviceViewModel!.selectService.asDriver().map({ $0?.title }).drive(serviceTitle).addDisposableTo(disposeBag)
    }
    
    private func createDurationViewModel() {
        self.selectDurationViewModel = SelectDurationViewModel(router: router)
        self.selectDurationViewModel!.duration.asDriver().map({ $0 == 0 ? nil : String($0) }).drive(duration).addDisposableTo(disposeBag)
    }
    
    func selectCategory() {
        if self.categoryViewModel == nil {
            createCategoryViewModel()
        }
        
        router.showSelectSeviceCategory(viewModel: self.categoryViewModel!)
    }
    
    func selectService() {
        if self.serviceViewModel == nil {
            createServiceViewModel()
        }
        
        router.showSelectSevice(viewModel: self.serviceViewModel!)
    }
    
    func selectDuration() {
        if self.selectDurationViewModel == nil {
            createDurationViewModel()
        }
        
        router.showSelectDuration(viewModel: self.selectDurationViewModel!)
    }
    
    func maySelectService() -> Bool {
        guard let categoryViewModel = self.categoryViewModel, let _ = categoryViewModel.selectCategory.value else {
            return false
        }
        
        return true
    }
    
    func remove() {
        delegate?.remove(service: service!)
        router.goBack()
    }
    
    func done() {
        
        if let cost = cost.value, let selectService = serviceViewModel?.selectService.value, let duration = selectDurationViewModel?.duration.value {

            if let service = service {
                service.cost = cost
                service.duration = duration
                service.service = selectService
                delegate?.edit(service: service)
            } else {
                let service = MasterService()
                service.cost = cost
                service.duration = duration
                service.service = selectService
                delegate?.add(service: service)
            }
            
            router.goBack()
        }
    }
}
