//
//  VCAddService.swift
//  Masters
//
//  Created by Andrew on 01.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VCAddService: UITableViewController {

    @IBOutlet weak var labelCategory: UILabel!
    @IBOutlet weak var labelTitleService: UILabel!
    @IBOutlet weak var labelService: UILabel!
    @IBOutlet weak var textFieldCost: UITextField!
    @IBOutlet weak var labelTime: UILabel!
    
    private let disposeBag = DisposeBag()
    
    var viewModel: AddServiceViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let buttonBarRemove = UIBarButtonItem.init(image: UIImage(named: "statusBarDelete"), style: .done, target: self, action: #selector(remove))
        let buttonBarDone = UIBarButtonItem.init(image: UIImage(named: "statusBarOk"), style: .done, target: self, action: #selector(done))
        
        self.navigationItem.rightBarButtonItems = viewModel.isAddService ? [buttonBarDone] : [buttonBarRemove, buttonBarDone]
        
        viewModel.categoryTitle.asDriver().drive(onNext: { [weak self] (title:String?) in
            if let labelCategory = self?.labelCategory {
                labelCategory.text = title
            }
            if let labelTitleService = self?.labelTitleService, let viewModel = self?.viewModel {
                labelTitleService.alpha = viewModel.maySelectService() ? 1 : 0.5
            }
        }).addDisposableTo(disposeBag)
        
        viewModel.serviceTitle.asDriver().drive(labelService.rx.text).addDisposableTo(disposeBag)
        
        if let defaultValue = viewModel.cost.value {
            textFieldCost.text = "\(defaultValue)"
        }
        
        textFieldCost.rx.text.asDriver().map{ Double($0!) }.drive(viewModel.cost).addDisposableTo(disposeBag)
        
        viewModel.duration.asDriver().drive(labelTime.rx.text).addDisposableTo(disposeBag)
    }
    
    // MARK: - Actions
    
    func remove() {
        viewModel.remove()
    }
    
    func done() {
        viewModel.done()
    }
    
    // MARK: -  TableView delegate
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == 0 && indexPath.row == 1 {
            return viewModel.maySelectService()
        } else if indexPath.section == 1 {
            return false
        }
        
        return true
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                viewModel.selectCategory()
            }
            if indexPath.row == 1 {
                viewModel.selectService()
            }
        } else if indexPath.section == 2 {
            viewModel.selectDuration()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
