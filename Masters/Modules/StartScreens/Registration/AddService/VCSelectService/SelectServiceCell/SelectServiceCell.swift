//
//  SelectServiceCell.swift
//  Masters
//
//  Created by Andrew on 11.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class SelectServiceCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!
    
    var data: ServiceViewModel! {
        didSet{
            labelTitle.text = data.serviceTitle
        }
    }

}
