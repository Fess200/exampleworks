//
//  ServiceViewModel.swift
//  Masters
//
//  Created by Andrew on 11.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class ServiceViewModel: NSObject {
    let service:Service
    
    var serviceTitle: String? {
        return service.title
    }
    
    init(service:Service) {
        self.service = service
    }
}
