//
//  SelectServiceViewModel.swift
//  Masters
//
//  Created by Andrew on 05.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SelectServiceViewModel: NSObject {
    
    private var appDataService: AppDataServiceProtocol!
    private let router:StartRouterProtocol
    
    private let ignoreServices:[Service]
    
    private var serviceViewModels: [ServiceViewModel]?
    
    private let categoryViewModel: SelectServiceCategoryViewModel
    
    let selectService: Variable<Service?> = Variable(nil)
    
    init(categoryViewModel: SelectServiceCategoryViewModel, router:StartRouterProtocol, ignoreServices:[Service]) {
        self.ignoreServices = ignoreServices
        self.categoryViewModel = categoryViewModel
        self.router = router
        appDataService = assembler.resolver.resolve(AppDataServiceProtocol.self)
    }
    
    func resetData() {
        serviceViewModels = nil
        selectService.value = nil
    }
    
    func requestData(complete:@escaping (()->())) {
        
        guard let _ = serviceViewModels else {
            appDataService.getServices(categoryId: categoryViewModel.selectCategory.value!.id, complete: { [weak self] (services:[Service]?) in
                guard let services = services else {
                    return
                }
                
                self?.serviceViewModels = services.filter({ (service:Service) -> Bool in
                    if let ignoreServices = self?.ignoreServices {
                        return !ignoreServices.contains(service)
                    } else {
                        return true
                    }
                })
                    
                    .map({ ServiceViewModel(service: $0) })
                complete()
            })
            return
        }
        
        complete()
    }
    
    func countData() -> Int {
        guard let serviceViewModels = self.serviceViewModels else {
            return 0
        }
        
        return serviceViewModels.count
    }
    
    func data(index:Int) -> ServiceViewModel {
        return serviceViewModels![index]
    }
    
    func select(index:Int) {
        selectService.value = serviceViewModels![index].service
        router.goBack()
    }
    
}
