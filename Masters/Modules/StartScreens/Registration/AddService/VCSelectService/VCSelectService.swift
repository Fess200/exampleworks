//
//  VCSelectService.swift
//  Masters
//
//  Created by Andrew on 05.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class VCSelectService: UITableViewController {

    private static let cellIdentifier = "SelectServiceCell"
    
    var viewModel:SelectServiceViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        viewModel.requestData { [weak self] in
            self?.tableView.reloadData()
        }
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.countData()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VCSelectService.cellIdentifier, for: indexPath) as! SelectServiceCell
        cell.data = viewModel.data(index: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.select(index: indexPath.row)
    }

}
