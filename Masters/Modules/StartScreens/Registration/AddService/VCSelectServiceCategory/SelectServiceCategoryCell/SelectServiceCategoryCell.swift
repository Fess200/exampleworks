//
//  SelectServiceCategoryCell.swift
//  Masters
//
//  Created by Andrew on 05.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class SelectServiceCategoryCell: UITableViewCell {

    @IBOutlet weak var labelTitle: UILabel!

    var data: ServiceCategoryViewModel! {
        didSet{
            labelTitle.text = data.categoryTitle
        }
    }

}
