//
//  ServiceCategoryViewModel.swift
//  Masters
//
//  Created by Andrew on 05.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class ServiceCategoryViewModel: NSObject {
    
    let category:ServiceCategory
    
    var categoryTitle: String? {
        return category.title
    }
    
    init(category:ServiceCategory) {
        self.category = category
    }
}
