//
//  SelectServiceCategoryViewModel.swift
//  Masters
//
//  Created by Andrew on 05.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class SelectServiceCategoryViewModel: NSObject {
    
    private var appDataService: AppDataServiceProtocol!
    private let router:StartRouterProtocol
    
    private var serviceCategoryViewModels: [ServiceCategoryViewModel]?
    
    let selectCategory: Variable<ServiceCategory?> = Variable(nil)
    
    init(router:StartRouterProtocol) {
        self.router = router
        appDataService = assembler.resolver.resolve(AppDataServiceProtocol.self)
    }
    
    func requestData(complete:@escaping (()->())) {
        
        guard let _ = serviceCategoryViewModels else {
            appDataService.getCategories { [weak self] (categories:[ServiceCategory]?) in
                
                guard let categories = categories else {
                    return
                }
                
                self?.serviceCategoryViewModels = categories.map({ ServiceCategoryViewModel(category: $0) })
                complete()
            }
            return
        }
        
        complete()
    }
    
    func countData() -> Int {
        guard let serviceCategoryViewModels = self.serviceCategoryViewModels else {
            return 0
        }
        
        return serviceCategoryViewModels.count
    }
    
    func data(index:Int) -> ServiceCategoryViewModel {
        return serviceCategoryViewModels![index]
    }
    
    func select(index:Int) {
        selectCategory.value = serviceCategoryViewModels![index].category
        router.goBack()
    }
    
}
