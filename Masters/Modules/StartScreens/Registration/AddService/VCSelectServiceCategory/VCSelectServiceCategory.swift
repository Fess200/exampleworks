//
//  VCSelectServiceCategory.swift
//  Masters
//
//  Created by Andrew on 05.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class VCSelectServiceCategory: UITableViewController {

    private static let cellIdentifier = "SelectServiceCategoryCell"
    
    var viewModel:SelectServiceCategoryViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.requestData { [weak self] in
            self?.tableView.reloadData()
        }
    }

    // MARK: - Table view data source

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.countData()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VCSelectServiceCategory.cellIdentifier, for: indexPath) as! SelectServiceCategoryCell
        cell.data = viewModel.data(index: indexPath.row)
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel.select(index: indexPath.row)
    }
    
}
