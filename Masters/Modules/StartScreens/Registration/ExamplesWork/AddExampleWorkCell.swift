//
//  AddExampleWorkCell.swift
//  Masters
//
//  Created by Andrew on 15.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class AddExampleWorkCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewAdd: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageViewAdd.layer.cornerRadius = 3
        imageViewAdd.layer.borderColor = UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1).cgColor
        imageViewAdd.layer.borderWidth = 1
        imageViewAdd.layer.masksToBounds = true
    }
}
