//
//  PreviewExampleWorkCell.swift
//  Masters
//
//  Created by Andrew on 15.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class PreviewExampleWorkCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewPreview: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        imageViewPreview.layer.cornerRadius = 3
        imageViewPreview.layer.borderColor = UIColor(red: 212.0/255.0, green: 212.0/255.0, blue: 212.0/255.0, alpha: 1).cgColor
        imageViewPreview.layer.borderWidth = 1
        imageViewPreview.layer.masksToBounds = true
    }
    
    var data: UIImage! {
        didSet {
            imageViewPreview.image = data
        }
    }
    
}
