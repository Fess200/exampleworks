//
//  PreviewExamplesWorkViewModel.swift
//  Masters
//
//  Created by Andrew on 12.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class PreviewExamplesWorkViewModel: NSObject {
    
    private let router:StartRouterProtocol
    
    var previews = Variable([UIImage]())
    
    init(router:StartRouterProtocol) {
        self.router = router
    }
    
    func countData() -> Int {
        return previews.value.count
    }
    
    func data(index:Int) -> UIImage {
        return previews.value[index]
    }
    
    func addPreview() {
        router.showSelectPhoto { [weak self] (image:UIImage?) in
            if let image = image {
                self?.previews.value.append(image)
            }
        }
    }
    
    func select(index:Int) {
        router.showDetailPreview(viewModel: DetailPreviewExampleWorkViewModel(router: router, image: previews.value[index]))
    }
    
}
