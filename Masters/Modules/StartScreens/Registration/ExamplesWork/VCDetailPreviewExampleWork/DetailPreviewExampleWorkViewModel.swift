//
//  DetailPreviewExampleWorkViewModel.swift
//  Masters
//
//  Created by Andrew on 14.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class DetailPreviewExampleWorkViewModel: NSObject {
    
    private let router:StartRouterProtocol
    
    let image: UIImage
    
    init(router:StartRouterProtocol, image:UIImage) {
        self.router = router
        self.image = image
    }
    
    func close() {
        router.dismiss()
    }
    
}
