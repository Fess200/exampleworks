//
//  VCDetailPreviewExampleWork.swift
//  Masters
//
//  Created by Andrew on 14.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class VCDetailPreviewExampleWork: UIViewController {

    @IBOutlet weak var imageViewPreview: UIImageView!
    
    var viewModel:DetailPreviewExampleWorkViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        imageViewPreview.image = viewModel.image
    }

    @IBAction func onClose(_ sender: Any) {
        viewModel.close()
    }

}
