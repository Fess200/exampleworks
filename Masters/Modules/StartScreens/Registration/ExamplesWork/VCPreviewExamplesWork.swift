//
//  VCPreviewExamplesWorks.swift
//  Masters
//
//  Created by Andrew on 15.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VCPreviewExamplesWork: UICollectionViewController {

    private static let identifierAddExampleWorkCell = "AddExampleWorkCell"
    private static let identifierPreviewExampleWorkCell = "PreviewExampleWorkCell"
    
    private let disposeBag = DisposeBag()
    
    var viewModel:PreviewExamplesWorkViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        viewModel.previews.asDriver().drive(onNext: { [weak self] (_) in
            self?.collectionView?.reloadData()
        }).addDisposableTo(disposeBag)
    }

    // MARK: UICollectionViewDataSource

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.countData()+1
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if indexPath.row == 0 {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VCPreviewExamplesWork.identifierAddExampleWorkCell, for: indexPath)
            return cell
        } else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VCPreviewExamplesWork.identifierPreviewExampleWorkCell, for: indexPath) as! PreviewExampleWorkCell
            cell.data = viewModel.data(index: indexPath.row-1)
            return cell
        }
    }

    // MARK: UICollectionViewDelegate

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            viewModel.addPreview()
        } else {
            viewModel.select(index: indexPath.row-1)
        }
    }

}
