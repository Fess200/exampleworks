//
//  MasterServiceCell.swift
//  Masters
//
//  Created by Andrew on 12.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class MasterServiceCell: UITableViewCell {

    @IBOutlet weak var viewContent: UIView!
    @IBOutlet weak var labelName: UILabel!
    @IBOutlet weak var labelDuration: UILabel!
    @IBOutlet weak var labelCost: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewContent.layer.cornerRadius = 2
    }

    var viewModel: MasterServiceViewModel! {
        didSet {
            labelName.text = viewModel.name
            labelDuration.text = "\(viewModel.duration) мин"
            labelCost.text = "\(viewModel.cost) Р"
        }
    }

}
