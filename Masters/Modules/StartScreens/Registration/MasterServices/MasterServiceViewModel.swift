//
//  MasterServiceViewModel.swift
//  Masters
//
//  Created by Andrew on 12.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class MasterServiceViewModel: NSObject {
    
    let service: MasterService
    
    var name: String? {
        return service.service.title
    }
    var duration: Int {
        return service.duration
    }
    var cost: Double {
        return service.cost
    }
    
    init(service: MasterService) {
        self.service = service
    }
    
}
