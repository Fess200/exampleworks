//
//  RegistrationViewModel.swift
//  Masters
//
//  Created by Andrew on 08.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RealmSwift
import RxRealm

class RegistrationViewModel: NSObject, AddServiceViewModelDelegate, ScheduleBreakViewModelDelegate {
    
    private let router:StartRouterProtocol
    private var tempScheduleBreakViewModel: ScheduleBreakViewModel?
    private let profileService: ProfileServiceProtocol
    private var region: Region? = nil {
        didSet {
            address.value = region?.name
        }
    }
    
    let previewExamplesWorkViewModel: PreviewExamplesWorkViewModel

    var isMaster = Variable(false)
    var familyName: Variable<String?> = Variable(nil)
    var nickName: Variable<String?> = Variable(nil)
    var phone: Variable<String?> = Variable(nil)
    var address: Variable<String?> = Variable(nil)
    var avatar: Variable<UIImage?> = Variable(nil)
    var about: Variable<String?> = Variable(nil)
    var serviceViewModels: Variable<[MasterServiceViewModel]> = Variable([])
    
    let scheduleTimeViewModel: ScheduleTimeViewModel!
    var scheduleBreakViewModel: Variable<[ScheduleBreakViewModel]>!
    let scheduleWeekendViewModel: ScheduleWeekendViewModel!
    
    let appDataService: AppDataServiceProtocol
    
    init(router:StartRouterProtocol, phone:String) {
        self.router = router
        self.phone.value = phone
        self.appDataService = assembler.resolver.resolve(AppDataServiceProtocol.self)!
        self.previewExamplesWorkViewModel = PreviewExamplesWorkViewModel(router: router)
        
        scheduleTimeViewModel = ScheduleTimeViewModel(router: router)
        scheduleWeekendViewModel = ScheduleWeekendViewModel(router: router)
        
        profileService = assembler.resolver.resolve(ProfileServiceProtocol.self)!
        
        super.init()
        
        scheduleBreakViewModel = Variable([createBreakPeriodInterval()])
    }
    
    func register() {
        if isMaster.value {
            if let familyName = familyName.value, let nickName = nickName.value, let phone = phone.value, let code = region?.code, let about = about.value {
                profileService.registerUser(name: familyName, nick: nickName, phone: correct(phone: phone), address: code, avatar: avatar.value, isMaster: isMaster.value, about: about, holidays: nil, breaks: nil, examplesWorks: nil, complete: { [weak self] (_, _) in
                    self?.router.changeToTabBarController()
                })
            }
        } else {
            if let familyName = familyName.value, let nickName = nickName.value, let phone = phone.value, let code = region?.code {
                profileService.registerUser(name: familyName, nick: nickName, phone: correct(phone: phone), address: code, avatar: avatar.value, isMaster: isMaster.value, about: nil, holidays: nil, breaks: nil, examplesWorks: nil, complete: { [weak self] (_, _) in
                    self?.router.changeToTabBarController()
                })
            }
        }
    }
    
    func changeRoleUser() {
        isMaster.value = !isMaster.value
    }
    
    func selectAvatar() {
        router.showSelectPhoto { [weak self] (image:UIImage?) in
            self?.avatar.value = image
        }
    }
    
    func editService(index:Int) {
        let viewModel = AddServiceViewModel(router: router, service: serviceViewModels.value[index].service, existServices: getExistServices())
        viewModel.delegate = self
        router.showAddService(viewModel: viewModel)
    }
    
    func addService() {
        let viewModel = AddServiceViewModel(router: router, service: nil, existServices: getExistServices())
        viewModel.delegate = self
        router.showAddService(viewModel: viewModel)
    }
    
    func selectAddress() {
        appDataService.getRegions { [weak self] (regions) in
            if let regViewModel = self {
                var indexSelect = 0
                if let region = regViewModel.region {
                    if let index = regions.index(where: { $0.code == region.code }) {
                        indexSelect = index
                    }
                }
                let viewModel = SelectDataViewModel(router: regViewModel.router, values: regions, indexSelect:indexSelect, convertBlock: { (region) -> String in
                    return (region as! Region).name
                })
                viewModel.selectedVariable.asObserver().subscribe(onNext: { (region) in
                    if let region = region as? Region {
                        regViewModel.region = region
                    }
                }).addDisposableTo(viewModel.disposeBag)
                regViewModel.router.showSelectData(viewModel: viewModel)
            }
        }
    }
    
    private func getExistServices() -> [Service] {
        return serviceViewModels.value.map{ $0.service.service }
    }
    
    // MARK:- MasterService
    
    func countMasterService() -> Int {
        return serviceViewModels.value.count
    }
    
    func dataMasterService(index:Int) -> MasterServiceViewModel {
        return serviceViewModels.value[index]
    }
    
    // MARK:- AddServiceViewModelDelegate
    
    func add(service:MasterService) {
        serviceViewModels.value.append(MasterServiceViewModel(service: service))
    }
    
    func edit(service:MasterService) {
        serviceViewModels.value = serviceViewModels.value
    }
    
    func remove(service:MasterService) {
        if let index = serviceViewModels.value.index(where: { $0.service == service }) {
            serviceViewModels.value.remove(at: index)
        }
    }
    
    // MARK:- PeriodInterval
    
    private func createBreakPeriodInterval() -> ScheduleBreakViewModel {
        let viewModel  = ScheduleBreakViewModel(router: router)
        viewModel.delegate = self
        return viewModel
    }
    
    func addingNewBreak() {
        tempScheduleBreakViewModel = createBreakPeriodInterval()
        tempScheduleBreakViewModel?.showEditSchedule()
    }
    
    func addedBreak(viewModel: ScheduleBreakViewModel) {
        var viewModels = scheduleBreakViewModel.value.filter { $0.timeInterval.value != nil }
        viewModels.append(viewModel)
        scheduleBreakViewModel.value = viewModels
    }
    
    func changedBreakPeriodInterval() {
        var viewModels = scheduleBreakViewModel.value.filter { $0.timeInterval.value != nil }
        if viewModels.count == 0 {
            viewModels.append(createBreakPeriodInterval())
        }
        scheduleBreakViewModel.value = viewModels
    }
}
