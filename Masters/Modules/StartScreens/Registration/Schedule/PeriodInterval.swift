//
//  PeriodInterval.swift
//  Masters
//
//  Created by Andrew on 20.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

struct PeriodDateInterval {
    let hours:Int
    let minutes:Int
}

struct PeriodInterval {
    var startPeriodInterval: PeriodDateInterval
    var endPeriodInterval: PeriodDateInterval
}
