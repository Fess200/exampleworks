//
//  ScheduleBreakCell.swift
//  Masters
//
//  Created by Andrew on 19.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ScheduleBreakCell: UITableViewCell {

    @IBOutlet weak var labelInterval: UILabel!
    @IBOutlet weak var buttonAdd: UIButton!
    
    private var disposable: Disposable?
    
    var viewModel:ScheduleBreakViewModel! {
        willSet {
            disposable?.dispose()
        }
        
        didSet {
            
            disposable = viewModel.timeInterval.asDriver().drive(onNext: { [weak self] (periodInterval:PeriodInterval?) in
                if let periodInterval = periodInterval {
                    if let startHours = self?.convertFormatNumberTime(time: periodInterval.startPeriodInterval.hours), let startMinutes = self?.convertFormatNumberTime(time: periodInterval.startPeriodInterval.minutes), let endHours = self?.convertFormatNumberTime(time: periodInterval.endPeriodInterval.hours), let endMinutes = self?.convertFormatNumberTime(time: periodInterval.endPeriodInterval.minutes) {
                        self?.labelInterval.text = "с \(startHours):\(startMinutes) по \(endHours):\(endMinutes)"
                    }
                } else {
                    self?.labelInterval.text = "Без перерыва"
                }
            })
            
        }
    }
    
    @IBAction func onEdit(_ sender: Any) {
        viewModel.editSchedule()
    }
    
    @IBAction func onAdd(_ sender: Any) {
        viewModel.addSchedule()
    }
    
    private func convertFormatNumberTime(time:Int) -> String {
        return time > 10 ? "\(time)" : "0\(time)"
    }

}
