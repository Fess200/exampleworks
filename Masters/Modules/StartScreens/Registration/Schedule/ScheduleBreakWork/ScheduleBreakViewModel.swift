//
//  ScheduleBreakViewModel.swift
//  Masters
//
//  Created by Andrew on 15.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

protocol ScheduleBreakViewModelDelegate: NSObjectProtocol {
    func addingNewBreak()
    func addedBreak(viewModel: ScheduleBreakViewModel)
    func changedBreakPeriodInterval()
}

class ScheduleBreakViewModel: NSObject, PeriodIntervalViewModelDelegate {
    
    private let router:StartRouterProtocol
    private let title = "Перерыв"
    
    var timeInterval: Variable<PeriodInterval?> = Variable(nil)
    
    weak var delegate: ScheduleBreakViewModelDelegate?
    
    init(router:StartRouterProtocol) {
        self.router = router
    }
    
    func defaultPeriodInterval() -> PeriodInterval {
        return PeriodInterval(startPeriodInterval: PeriodDateInterval(hours: 0, minutes: 0), endPeriodInterval: PeriodDateInterval(hours: 0, minutes: 0))
    }
    
    func showEditSchedule() {
        let viewModel = PeriodIntervalViewModel(router: router, periodInterval: timeInterval.value, title: title)
        viewModel.delegate = self
        router.showEditPeriodInterval(viewModel: viewModel)
    }
    
    func editSchedule() {
        if timeInterval.value == nil {
            delegate?.addingNewBreak()
        } else {
            showEditSchedule()
        }
    }
    
    func addSchedule() {
        delegate?.addingNewBreak()
    }
    
    //MARK - PeriodIntervalViewModelDelegate
    
    func add(periodInterval: PeriodInterval) {
        timeInterval.value = periodInterval
        delegate?.addedBreak(viewModel: self)
    }
    
    func edit(periodInterval: PeriodInterval) {
        timeInterval.value = periodInterval
        delegate?.changedBreakPeriodInterval()
    }
    
    func remove(periodInterval: PeriodInterval) {
        timeInterval.value = nil
        delegate?.changedBreakPeriodInterval()
    }
    
}
