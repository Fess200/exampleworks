//
//  ScheduleTimeCell.swift
//  Masters
//
//  Created by Andrew on 01.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ScheduleTimeCell: UITableViewCell {
    
    @IBOutlet weak var labelInterval: UILabel!
    
    private var disposable: Disposable?
    
    var viewModel:ScheduleTimeViewModel! {
        
        willSet {
            disposable?.dispose()
        }
        
        didSet {
            
            disposable = viewModel.scheduleInterval.asDriver().drive(onNext: { [weak self] (periodInterval:PeriodInterval?) in
                if let periodInterval = periodInterval {
                    if let startHours = self?.convertFormatNumberTime(time: periodInterval.startPeriodInterval.hours), let startMinutes = self?.convertFormatNumberTime(time: periodInterval.startPeriodInterval.minutes), let endHours = self?.convertFormatNumberTime(time: periodInterval.endPeriodInterval.hours), let endMinutes = self?.convertFormatNumberTime(time: periodInterval.endPeriodInterval.minutes) {
                        self?.labelInterval.text = "с \(startHours):\(startMinutes) по \(endHours):\(endMinutes)"
                    }
                } else {
                    self?.labelInterval.text = "Круглосуточно"
                }
            })
            
        }
    }
    
    private func convertFormatNumberTime(time:Int) -> String {
        return time > 10 ? "\(time)" : "0\(time)"
    }

    @IBAction func onEdit(_ sender: Any) {
        viewModel.showEditSchedule()
    }
}
