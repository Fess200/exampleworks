//
//  ScheduleTimeViewModel.swift
//  Masters
//
//  Created by Andrew on 15.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class ScheduleTimeViewModel: NSObject, PeriodIntervalViewModelDelegate {
    
    private let router:StartRouterProtocol
    private let title = "Время работы"
    
    var scheduleInterval: Variable<PeriodInterval?> = Variable(nil)
    
    init(router:StartRouterProtocol) {
        self.router = router
    }
    
    func defaultPeriodInterval() -> PeriodInterval {
        return PeriodInterval(startPeriodInterval: PeriodDateInterval(hours: 0, minutes: 0), endPeriodInterval: PeriodDateInterval(hours: 0, minutes: 0))
    }
    
    func showEditSchedule() {
        let viewModel = PeriodIntervalViewModel(router: router, periodInterval: scheduleInterval.value, title: title)
        viewModel.delegate = self
        router.showEditPeriodInterval(viewModel: viewModel)
    }
    
    //MARK - PeriodIntervalViewModelDelegate
    
    func add(periodInterval: PeriodInterval) {
        scheduleInterval.value = periodInterval
    }
    
    func edit(periodInterval: PeriodInterval) {
        scheduleInterval.value = periodInterval
    }
    
    func remove(periodInterval: PeriodInterval) {
        scheduleInterval.value = nil
    }
    
}
