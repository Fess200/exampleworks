//
//  ScheduleWeekendCell.swift
//  Masters
//
//  Created by Andrew on 19.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class ScheduleWeekendCell: UITableViewCell {

    @IBOutlet weak var labelDays: UILabel!
    
    private var disposable: Disposable?
    
    var viewModel:ScheduleWeekendViewModel! {
        willSet {
            disposable?.dispose()
        }
        
        didSet {
            
            disposable = viewModel.daysWeekend.asDriver().drive(onNext: { [weak self] (daysWeekend:[DayWeekend]) in
                if daysWeekend.count == 0 {
                    self?.labelDays.text = "Без выходных"
                } else {
                    var days = String()
                    for dayWeekend in daysWeekend {
                        days.append(dayWeekend.rawValue)
                        
                        if dayWeekend != daysWeekend.last {
                            days.append(", ")
                        }
                    }
                    
                    self?.labelDays.text = days
                }
            })
            
        }
    }
    
    @IBAction func onEdit(_ sender: Any) {
        viewModel.showEditDaysWeekend()
    }

}
