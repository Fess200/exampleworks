//
//  ScheduleWeekendViewModel.swift
//  Masters
//
//  Created by Andrew on 19.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class ScheduleWeekendViewModel: NSObject, WeekendDaysViewModelDelegate {
    
    private let router:StartRouterProtocol
    
    var daysWeekend: Variable<[DayWeekend]>
    
    init(router:StartRouterProtocol) {
        self.router = router
        
        self.daysWeekend = Variable([.saturday, .sunday])
    }
    
    func showEditDaysWeekend() {
        let viewModel = WeekendDaysViewModel(router: router, daysWeekendSelected: daysWeekend.value)
        viewModel.delegate = self
        router.showEditDaysWeekend(viewModel: viewModel)
    }
    
    // MARK:- WeekendDaysViewModelDelegate
    
    func changeServices(daysWeekend: [DayWeekend]) {
        self.daysWeekend.value = daysWeekend
    }

}
