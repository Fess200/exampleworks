//
//  IntervalTimeViewModel.swift
//  Masters
//
//  Created by Andrew on 26.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

protocol PeriodIntervalViewModelDelegate: NSObjectProtocol {
    func add(periodInterval: PeriodInterval)
    func edit(periodInterval: PeriodInterval)
    func remove(periodInterval: PeriodInterval)
}

class PeriodIntervalViewModel: NSObject {
    
    private var periodInterval: PeriodInterval?
    private let router:StartRouterProtocol
    
    private let disposeBag = DisposeBag()
    
    var startPeriodInterval: Variable<PeriodDateInterval>
    var endPeriodInterval: Variable<PeriodDateInterval>
    var title: String
    
    weak var delegate: PeriodIntervalViewModelDelegate?
    
    var isAddInterval: Bool {
        return periodInterval == nil
    }
    
    init(router:StartRouterProtocol, periodInterval: PeriodInterval?, title: String) {
        self.router = router
        self.periodInterval = periodInterval
        self.title = title
        
        if let periodInterval = periodInterval {
            startPeriodInterval = Variable(periodInterval.startPeriodInterval)
            endPeriodInterval = Variable(periodInterval.endPeriodInterval)
        } else {
            startPeriodInterval = Variable(PeriodDateInterval(hours: 0, minutes: 0))
            endPeriodInterval = Variable(PeriodDateInterval(hours: 0, minutes: 0))
        }
    }
    
    func remove() {
        delegate?.remove(periodInterval: periodInterval!)
        router.goBack()
    }
    
    func done() {
        if var periodInterval = periodInterval {
            periodInterval.startPeriodInterval = startPeriodInterval.value
            periodInterval.endPeriodInterval = endPeriodInterval.value
            delegate?.edit(periodInterval: periodInterval)
        } else {
            let periodInterval = PeriodInterval(startPeriodInterval: startPeriodInterval.value, endPeriodInterval: endPeriodInterval.value)
            delegate?.add(periodInterval: periodInterval)
        }
        
        router.goBack()
    }
    
    func selectStartPeriodInterval() {
        let viewModel = SelectPeriodIntervalViewModel(router: router, periodInterval: startPeriodInterval.value)
        viewModel.periodInterval.asDriver().skip(1).drive(startPeriodInterval).addDisposableTo(disposeBag)
        router.showSelectPeriodInterval(viewModel: viewModel)
    }
    
    func selectEndPeriodInterval() {
        let viewModel = SelectPeriodIntervalViewModel(router: router, periodInterval: startPeriodInterval.value)
        viewModel.periodInterval.asDriver().skip(1).drive(endPeriodInterval).addDisposableTo(disposeBag)
        router.showSelectPeriodInterval(viewModel: viewModel)
    }
}
