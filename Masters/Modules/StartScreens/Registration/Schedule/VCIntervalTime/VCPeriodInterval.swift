//
//  VCPeriodInterval.swift
//  Masters
//
//  Created by Andrew on 26.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class VCPeriodInterval: UITableViewController {

    @IBOutlet weak var labelStart: UILabel!
    @IBOutlet weak var labelEnd: UILabel!
    
    private let disposeBag = DisposeBag()
    
    var viewModel: PeriodIntervalViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = viewModel.title
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        let buttonBarRemove = UIBarButtonItem.init(image: UIImage(named: "statusBarDelete"), style: .done, target: self, action: #selector(remove))
        let buttonBarDone = UIBarButtonItem.init(image: UIImage(named: "statusBarOk"), style: .done, target: self, action: #selector(done))
        
        self.navigationItem.rightBarButtonItems = viewModel.isAddInterval ? [buttonBarDone] : [buttonBarRemove, buttonBarDone]
        
        viewModel.startPeriodInterval.asDriver().drive(onNext: { [weak self] periodDateInterval in
            if let hours = self?.convertFormatNumberTime(time: periodDateInterval.hours), let minutes = self?.convertFormatNumberTime(time: periodDateInterval.minutes) {
                self?.labelStart.text = "Начало в \(hours):\(minutes)"
            }
        }).addDisposableTo(disposeBag)
        
        viewModel.endPeriodInterval.asDriver().drive(onNext: { [weak self] periodDateInterval in
            if let hours = self?.convertFormatNumberTime(time: periodDateInterval.hours), let minutes = self?.convertFormatNumberTime(time: periodDateInterval.minutes) {
                self?.labelEnd.text = "Окончание в \(hours):\(minutes)"
            }
        }).addDisposableTo(disposeBag)
    }
    
    private func convertFormatNumberTime(time:Int) -> String {
        return time > 10 ? "\(time)" : "0\(time)"
    }
    
    // MARK: - Actions
    
    func remove() {
        viewModel.remove()
    }
    
    func done() {
        viewModel.done()
    }
    
    // MARK: -  TableView delegate
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            viewModel.selectStartPeriodInterval()
        }
        if indexPath.row == 1 {
            viewModel.selectEndPeriodInterval()
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }

}
