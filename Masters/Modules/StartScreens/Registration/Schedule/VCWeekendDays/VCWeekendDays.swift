//
//  VCWeekendDays.swift
//  Masters
//
//  Created by Andrew on 09.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class VCWeekendDays: UITableViewController {

    private static let identifierCell = "WeekendDaysCell"
    
    var viewModel: WeekendDaysViewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let buttonBarRemove = UIBarButtonItem.init(image: UIImage(named: "statusBarDelete"), style: .done, target: self, action: #selector(remove))
        let buttonBarDone = UIBarButtonItem.init(image: UIImage(named: "statusBarOk"), style: .done, target: self, action: #selector(done))
        
        self.navigationItem.rightBarButtonItems = [buttonBarRemove, buttonBarDone]
        
        let indexes = viewModel.getSelectedIndexes().map({ IndexPath(row: $0, section: 0) })
        for index in indexes {
            tableView.selectRow(at: index, animated: true, scrollPosition: .none)
        }
    }
    
    // MARK: - Actions
    
    func remove() {
        viewModel.remove()
    }
    
    func done() {
        viewModel.done()
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.countData
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: VCWeekendDays.identifierCell, for: indexPath) as! WeekendDaysCell
        cell.viewModel = viewModel.data(index: indexPath.row)
        cell.isLastCell = indexPath.row == viewModel.countData-1
        return cell
    }

}
