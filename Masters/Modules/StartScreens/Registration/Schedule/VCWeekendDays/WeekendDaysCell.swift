//
//  WeekendDaysCell.swift
//  Masters
//
//  Created by Andrew on 09.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class WeekendDaysCell: UITableViewCell {

    @IBOutlet weak var labelDay: UILabel!
    @IBOutlet weak var imageViewCheck: UIImageView!
    @IBOutlet weak var viewLineBottom: UIView!

    private var disposable: Disposable?
    
    var viewModel: WeekendDaysCellViewModel! {
        willSet {
            disposable?.dispose()
        }
        didSet {
            labelDay.text = viewModel.dayName
            disposable = viewModel.isSelected.asDriver().drive(onNext: { [weak self] isSelected in
                self?.imageViewCheck.isHidden = !isSelected
                self?.labelDay.textColor = isSelected ? UIColor(red: 76.0/255.0, green: 181.0/255.0, blue: 171.0/255.0, alpha: 1) : UIColor.black
            })
        }
    }
    
    var isLastCell: Bool! {
        didSet {
            viewLineBottom.isHidden = !isLastCell
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if let viewModel = viewModel {
            viewModel.isSelected.value = isSelected
        }
    }

}
