//
//  WeekendDaysCellViewModel.swift
//  Masters
//
//  Created by Andrew on 09.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

class WeekendDaysCellViewModel: NSObject {
    
    let dayWeekend: DayWeekend
    
    var dayName: String {
        return dayWeekend.rawValue
    }
    
    let isSelected: Variable<Bool>
    
    init(dayWeekend:DayWeekend, isSelected: Bool) {
        self.dayWeekend = dayWeekend
        self.isSelected = Variable(isSelected)
    }
    
}
