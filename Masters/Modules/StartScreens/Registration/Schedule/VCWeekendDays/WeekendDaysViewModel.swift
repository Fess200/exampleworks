//
//  WeekendDaysViewModel.swift
//  Masters
//
//  Created by Andrew on 09.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift

protocol WeekendDaysViewModelDelegate: NSObjectProtocol {
    func changeServices(daysWeekend: [DayWeekend])
}

class WeekendDaysViewModel: NSObject {
    
    private let daysWeekend: [DayWeekend] = [.monday, .tuesday, .wednesday, .thursday, .friday, .saturday, .sunday]
    private let router:StartRouterProtocol
    
    let daysWeekendViewModels: Variable<[WeekendDaysCellViewModel]>
    
    weak var delegate: WeekendDaysViewModelDelegate?
    
    var countData: Int {
        return daysWeekend.count
    }
    
    init(router:StartRouterProtocol, daysWeekendSelected: [DayWeekend]) {
        self.router = router
        
        var daysWeekendViewModels = [WeekendDaysCellViewModel]()
        daysWeekend.forEach { daysWeekendViewModels.append(WeekendDaysCellViewModel(dayWeekend: $0, isSelected: daysWeekendSelected.contains($0) )) }
        self.daysWeekendViewModels = Variable(daysWeekendViewModels)
    }
    
    func data(index: Int) -> WeekendDaysCellViewModel {
        return self.daysWeekendViewModels.value[index]
    }
    
    func getSelectedIndexes() -> [Int] {
        var indexes = [Int]()
        
        for index in 0..<daysWeekendViewModels.value.count {
            let daysWeekendViewModel = data(index: index)
            if daysWeekendViewModel.isSelected.value {
                indexes.append(index)
            }
        }
        
        return indexes
    }
    
    private func getSelectedDaysWeekend() -> [DayWeekend] {
        return daysWeekendViewModels.value.filter { $0.isSelected.value }.map { $0.dayWeekend }
    }
    
    func remove() {
        delegate?.changeServices(daysWeekend: [])
        router.goBack()
    }
    
    func done() {
        delegate?.changeServices(daysWeekend: getSelectedDaysWeekend())
        router.goBack()
    }
}
