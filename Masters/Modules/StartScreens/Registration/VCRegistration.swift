//
//  VCRegistration.swift
//  Masters
//
//  Created by Andrew on 08.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class VCRegistration: UITableViewController, UITextFieldDelegate {

    private static let identifierMasterServiceCell = "MasterServiceCell"
    private static let identifierScheduleTimeCell = "ScheduleTimeCell"
    private static let identifierScheduleBreakCell = "ScheduleBreakCell"
    private static let identifierScheduleWeekendCell = "ScheduleWeekendCell"
    
    private static let sectionCommonIndex = 0
    private static let sectionServicesIndex = 2
    private static let sectionScheduleIndex = 4
    
    private static let rowAddressIndex = 2
    
    private static let segueEmbedExamplesWork = "segueEmbedExamplesWork"
    
    @IBOutlet weak var textFieldFamilyName: UITextField!
    @IBOutlet weak var textFieldNickName: UITextField!
    @IBOutlet weak var textFieldPhone: WTReTextField!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var textFieldAbout: UITextField!
    
    @IBOutlet weak var buttonMaster: CheckBox!
    @IBOutlet weak var buttonAvatar: UIButton!
    @IBOutlet weak var buttonAddService: UIButton!
    
    @IBOutlet var viewHeaderServices: UIView!
    @IBOutlet var viewFooterServices: UIView!
    @IBOutlet var viewHeaderSchedule: UIView!
    
    @IBOutlet weak var layoutWidthButtonAvatar: NSLayoutConstraint!
    
    var viewModel:RegistrationViewModel! = nil
    
    private let disposeBag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        textFieldPhone.pattern = Constants.phoneRegex
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title:"", style:.plain, target:nil, action:nil)
        
        buttonAvatar.layer.cornerRadius = layoutWidthButtonAvatar.constant/2
        buttonAvatar.layer.borderColor = UIColor.white.cgColor
        buttonAvatar.layer.borderWidth = 4
        buttonAvatar.layer.masksToBounds = true
        
        buttonAddService.layer.cornerRadius = 3
        
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(title: "Готово", style: .done, target: self, action: #selector(onRegister))
        
        self.tableView.register(UINib(nibName: "MasterServiceCell", bundle: nil), forCellReuseIdentifier: VCRegistration.identifierMasterServiceCell)
        self.tableView.register(UINib(nibName: "ScheduleTimeCell", bundle: nil), forCellReuseIdentifier: VCRegistration.identifierScheduleTimeCell)
        self.tableView.register(UINib(nibName: "ScheduleBreakCell", bundle: nil), forCellReuseIdentifier: VCRegistration.identifierScheduleBreakCell)
        self.tableView.register(UINib(nibName: "ScheduleWeekendCell", bundle: nil), forCellReuseIdentifier: VCRegistration.identifierScheduleWeekendCell)
        
        viewModel.isMaster.asDriver().drive(onNext: { [weak self] (isMaster:Bool) in
            self?.buttonMaster.isSelect = isMaster
            self?.tableView.reloadData()
        }).addDisposableTo(disposeBag)
        
        textFieldFamilyName.rx.text.asDriver().drive(viewModel.familyName).addDisposableTo(disposeBag)
        textFieldNickName.rx.text.asDriver().drive(viewModel.nickName).addDisposableTo(disposeBag)
        textFieldPhone.text = viewModel.phone.value
        textFieldPhone.rx.text.asDriver().drive(viewModel.phone).addDisposableTo(disposeBag)
        
        viewModel.avatar.asDriver().drive(onNext: { [weak self] (image:UIImage?) in
            if image != nil {
                self?.buttonAvatar.setImage(image, for: .normal)
            }
        }).addDisposableTo(disposeBag)
        
        viewModel.serviceViewModels.asDriver().skip(1).drive(onNext: { [weak self] (_) in
            self?.tableView.reloadData()
        }).addDisposableTo(disposeBag)
        
        viewModel.scheduleBreakViewModel.asDriver().skip(1).drive(onNext: { [weak self] (_) in
            self?.tableView.reloadData()
        }).addDisposableTo(disposeBag)
        
        viewModel.address.asDriver().drive(onNext: { [weak self] (address) in
            if let address = address {
                self?.labelAddress.text = address
                self?.labelAddress.textColor = UIColor.black
            } else {
                self?.labelAddress.text = "Адрес"
                self?.labelAddress.textColor = UIColor(red: 199.0/255.0, green: 199.0/255.0, blue: 199.0/255.0, alpha: 1)
            }
        }).addDisposableTo(disposeBag)
        
        textFieldAbout.rx.text.asDriver().drive(viewModel.about).addDisposableTo(disposeBag)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.navigationController?.setNavigationBarHidden(false, animated: true)
    }
    
    // MARK: - Actions
    
    func onRegister() {
        viewModel.register()
    }
    
    @IBAction func onAddService(_ sender: Any) {
        viewModel.addService()
    }
    
    @IBAction func onChangeAvatar(_ sender: Any) {
        viewModel.selectAvatar()
    }
    
    @IBAction func onMaster(_ sender: Any) {
        viewModel.changeRoleUser()
    }
    
    // MARK: - UITableViewDelegate
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == VCRegistration.sectionServicesIndex {
            return viewHeaderServices.frame.size.height
        } else if section == VCRegistration.sectionScheduleIndex {
            return viewHeaderSchedule.frame.size.height
        } else {
            return 0.001
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == VCRegistration.sectionServicesIndex {
            return viewFooterServices.frame.size.height
        } else {
            return 0.001
        }
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == VCRegistration.sectionServicesIndex {
            return 60
        } else if indexPath.section == VCRegistration.sectionScheduleIndex {
            return 43
        } else {
            return super.tableView(tableView, heightForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == VCRegistration.sectionServicesIndex {
            return viewHeaderServices
        } else if section == VCRegistration.sectionScheduleIndex {
            return viewHeaderSchedule
        } else {
            return nil
        }
    }
    
    override func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        if section == VCRegistration.sectionServicesIndex {
            return viewFooterServices
        } else {
            return nil
        }
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.isMaster.value ? 5 : 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == VCRegistration.sectionServicesIndex {
            return viewModel.countMasterService()
        } else if section == VCRegistration.sectionScheduleIndex {
            return viewModel.scheduleBreakViewModel.value.count+2
        } else {
            return super.tableView(tableView, numberOfRowsInSection: section)
        }
    }
    
    override func tableView(_ tableView: UITableView, shouldHighlightRowAt indexPath: IndexPath) -> Bool {
        if indexPath.section == VCRegistration.sectionServicesIndex {
            return true
        } else if indexPath.section == VCRegistration.sectionCommonIndex && indexPath.row == VCRegistration.rowAddressIndex {
            return true
        }
        
        return false
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == VCRegistration.sectionServicesIndex {
            let cell = tableView.dequeueReusableCell(withIdentifier: VCRegistration.identifierMasterServiceCell, for: indexPath) as! MasterServiceCell
            cell.viewModel = viewModel.dataMasterService(index: indexPath.row)
            return cell
        } else if indexPath.section == VCRegistration.sectionScheduleIndex {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: VCRegistration.identifierScheduleTimeCell, for: indexPath) as! ScheduleTimeCell
                cell.viewModel = viewModel.scheduleTimeViewModel
                return cell
            } else if indexPath.row != self.tableView(tableView, numberOfRowsInSection: indexPath.section)-1 {
                let cell = tableView.dequeueReusableCell(withIdentifier: VCRegistration.identifierScheduleBreakCell, for: indexPath) as! ScheduleBreakCell
                cell.viewModel = viewModel.scheduleBreakViewModel.value[indexPath.row-1]
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: VCRegistration.identifierScheduleWeekendCell, for: indexPath) as! ScheduleWeekendCell
                cell.viewModel = viewModel.scheduleWeekendViewModel
                return cell
            }
        } else {
            return super.tableView(tableView, cellForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, indentationLevelForRowAt indexPath: IndexPath) -> Int {
        if indexPath.section == VCRegistration.sectionServicesIndex || indexPath.section == VCRegistration.sectionScheduleIndex {
            return 0
        } else {
            return super.tableView(tableView, indentationLevelForRowAt: indexPath)
        }
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == VCRegistration.sectionServicesIndex {
            viewModel.editService(index: indexPath.row)
        } else if indexPath.section == VCRegistration.sectionCommonIndex && indexPath.row == VCRegistration.rowAddressIndex {
            return viewModel.selectAddress()
        } else {
            super.tableView(tableView, didSelectRowAt: indexPath)
        }
    }
    
    // MARK: - UITextFieldDelegate
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == VCRegistration.segueEmbedExamplesWork {
            let controller = segue.destination as! VCPreviewExamplesWork
            controller.viewModel = viewModel.previewExamplesWorkViewModel
        }
    }
}
