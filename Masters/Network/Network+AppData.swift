//
//  Network+AppData.swift
//  Masters
//
//  Created by Andrew on 05.02.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

extension Network: AppDataNetworkProtocol {
    
    func getVolcabuary(complete: @escaping ((_ volcabuary:Volcabuary?, _ error:Error?)->())) {
        manager.request(urlApiString(path: "volcabuary"), method: .get).responseObject { (response:DataResponse<Volcabuary>) in
            complete(response.result.value, response.result.error)
        }
    }
    
}
