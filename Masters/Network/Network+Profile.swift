//
//  Network+Profile.swift
//  Masters
//
//  Created by Andrew on 06.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

extension Network: ProfileNetworkProtocol {
    
    func sendCodeSMS(phone:String, complete: @escaping ((_ success:Bool, _ error:NetworkError?)->())) -> URLSessionTask? {
        return manager.request(urlApiString(path: "send-code"), method: .post, parameters: ["phone":phone]).responseObject { (response: DataResponse<NetworkResponse>) in
            complete(response.result.value?.success ?? false, response.result.value?.error)
        }.task
    }
    
    func registerUser(name:String, nick:String, phone:String, address:String, avatar: UIImage?, isMaster: Bool, about:String?, holidays: [DayWeekend]?, breaks: [PeriodInterval]?, examplesWorks: [UIImage]?, complete: @escaping ((_ user:User?, _ error:NetworkError?)->())) {
        var parameters: [String: Any] = ["User[username]":name,"User[nickname]":nick,"User[phone]":phone,"User[address]":address, "User[is_master]": isMaster]
        if let avatar = avatar {
            parameters["User[vImage]"] = avatar
        }
        if let about = about {
            parameters["User[about]"] = about
        }
        if let holidays = holidays {
            parameters["User[holidays][]"] = holidays.map { $0.numberDay() }
        }
        if let breaks = breaks {
            parameters["User[vBreaks][t_start][]"] = breaks.map({ (periodInterval) -> String in
                let minutes = periodInterval.startPeriodInterval.minutes
                let minutesString = minutes < 10 ? "0\(minutes)" : "\(minutes)"
                return "\(periodInterval.startPeriodInterval.hours):\(minutesString)"
            })
            parameters["User[vBreaks][t_finish][]"] = breaks.map({ (periodInterval) -> String in
                let minutes = periodInterval.endPeriodInterval.minutes
                let minutesString = minutes < 10 ? "0\(minutes)" : "\(minutes)"
                return "\(periodInterval.endPeriodInterval.hours):\(minutesString)"
            })
        }
        if let examplesWorks = examplesWorks {
            parameters["User[vWorks][vImage][]"] = examplesWorks
        }
        
        manager.request(urlApiString(path: "reg-user"), method: .post, parameters: parameters).responseObject { (response:DataResponse<User>) in
            if let user = response.result.value {
                complete(user, nil)
            } else {
                complete(nil, nil)
            }
        }
    }
    
}
