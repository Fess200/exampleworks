//
//  Network+Searching.swift
//  Masters
//
//  Created by Andrew on 27.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import Alamofire
import AlamofireObjectMapper

extension Network: SearchNetworkProtocol {
    
    func search(token: String, nickname: String, cityId:String, serviceIds: [Int], rating: Int?, radius: CGFloat?, costFrom: Int?, costTo: Int?, complete: @escaping ((_ volcabuary:Volcabuary?, _ error:Error?)->())) {
        
        var parameters: [String: Any] = ["nickname":nickname,"city_id":cityId,"service_id[]":serviceIds]
        if let rating = rating {
            parameters["rating"] = rating
        }
        if let radius = radius {
            parameters["radius"] = radius
        }
        if let costFrom = costFrom {
            parameters["cost_from"] = costFrom
        }
        if let costTo = costTo {
            parameters["cost_to"] = costTo
        }
        
        manager.request(urlApiString(path: "master-find"), method: .get, parameters: parameters, headers: ["token":token]).responseJSON { (response:DataResponse<Any>) in
            let dict = try! JSONSerialization.jsonObject(with: response.data!, options: .mutableLeaves)
            print("\(dict)")
        }
    }
    
}
