//
//  Network.swift
//  Masters
//
//  Created by Andrew on 05.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireNetworkActivityIndicator

class Network: NSObject {
    
    let manager: SessionManager
    
    override init() {
        
        NetworkActivityIndicatorManager.shared.isEnabled = true

        manager = Alamofire.SessionManager()
        
        super.init()
    }
    
    func urlApiString(path:String) -> String {
        return urlString(path: "api/\(path)")
    }
    
    func urlString(path:String) -> String {
        return "http://144.76.174.174/\(path)"
    }
    
}
