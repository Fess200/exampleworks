//
//  NetworkError.swift
//  Masters
//
//  Created by Andrew on 08.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import ObjectMapper

class NetworkError: Mappable {
    
    var code:Int?
    var message:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        code <- map["code"]
        message <- map["m"]
    }
    
}
