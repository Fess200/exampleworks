//
//  NetworkResponse.swift
//  Masters
//
//  Created by Andrew on 08.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import ObjectMapper

class NetworkResponse: Mappable {
    
    var success:Bool?
    var error:NetworkError?
    
    required init?(map: Map) {
    }

    func mapping(map: Map) {
        success <- map["success"]
        error <- map["error"]
    }
}
