//
//  HomeRouter.swift
//  Masters
//
//  Created by Andrew on 25.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class HomeRouter: NSObject, HomeRouterProtocol {
    
    var rootController: UIViewController {
        let controller = UIStoryboard.home().instantiateInitialViewController() as! UINavigationController
        (controller.viewControllers.first as! VCHome).viewModel = HomeViewModel(router: self)
        return controller
    }
    
}
