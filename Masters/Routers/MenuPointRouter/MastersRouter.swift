//
//  MastersRouter.swift
//  Masters
//
//  Created by Andrew on 25.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class MastersRouter: NSObject, MastersRouterProtocol {
    
    var rootController: UIViewController {
        return UIStoryboard.masters().instantiateInitialViewController()!
    }
    
}
