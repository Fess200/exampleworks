//
//  ProfileRouter.swift
//  Masters
//
//  Created by Andrew on 25.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class ProfileRouter: NSObject, ProfileRouterProtocol {

    private let profileService: ProfileServiceProtocol
    private var navigationController: UINavigationController!
    
    var rootController: UIViewController {
        navigationController = UIStoryboard.profile().instantiateInitialViewController() as! UINavigationController
        if self.profileService.isMaster() {
            let controller = UIStoryboard.profile().instantiateViewController(withIdentifier: "VCMasterProfile") as! VCMasterProfile
            controller.viewModel = ProfileViewModel()
            navigationController.setViewControllers([controller], animated: false)
        } else {
            let controller = UIStoryboard.profile().instantiateViewController(withIdentifier: "VCUserProfile") as! VCUserProfile
            controller.viewModel = ProfileViewModel()
            navigationController.setViewControllers([controller], animated: false)
        }
        return navigationController
    }
    
    init(profileService:ProfileServiceProtocol) {
        self.profileService = profileService
        super.init()
    }
    
}
