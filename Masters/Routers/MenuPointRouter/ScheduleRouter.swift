//
//  ScheduleRouter.swift
//  Masters
//
//  Created by Andrew on 25.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class ScheduleRouter: NSObject, ScheduleRouterProtocol {

    var rootController: UIViewController {
        return UIStoryboard.schedule().instantiateInitialViewController()!
    }

}
