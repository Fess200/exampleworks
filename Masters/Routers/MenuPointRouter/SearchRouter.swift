//
//  SearchRouter.swift
//  Masters
//
//  Created by Andrew on 25.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class SearchRouter: NSObject, SearchRouterProtocol {

    private var navigationController: UINavigationController!
    
    var rootController: UIViewController {
        navigationController = UIStoryboard.search().instantiateInitialViewController() as! UINavigationController
        (navigationController.viewControllers.first as! VCSearch).viewModel = SearchViewModel(router: self)
        return navigationController
    }
    
    func showSelectData(viewModel:SelectDataViewModel) {
        let controller = UIStoryboard.storyBoardCommon().instantiateViewController(withIdentifier: "SelectDataVC") as! SelectDataVC
        controller.viewModel = viewModel
        controller.modalPresentationStyle = .overFullScreen
        navigationController.present(controller, animated: true, completion: nil)
    }
    
    func showSearchSelectService(viewModel:SearchSelectServiceViewModel) {
        let controller = UIStoryboard.search().instantiateViewController(withIdentifier: "VCSearchSelectService") as! VCSearchSelectService
        controller.viewModel = viewModel
        navigationController.pushViewController(controller, animated: true)
    }
    
    func dismiss() {
        navigationController.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func goBack() {
        navigationController.popViewController(animated: true)
    }

}
