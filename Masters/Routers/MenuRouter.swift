//
//  MenuRouter.swift
//  Masters
//
//  Created by Andrew on 05.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import Foundation

class MenuRouter: NSObject, MenuRouterProtocol {
    
    private let profileService: ProfileServiceProtocol
    private var routers: [MenuPointRouterProtocol]?
    
    init(profileService:ProfileServiceProtocol) {
        self.profileService = profileService
        super.init()
    }
    
    var rootController: UIViewController {
        let tabBarController = UITabBarController()
        var routers = [MenuPointRouterProtocol]()
        if self.profileService.isMaster() {
            routers.append(assembler.resolver.resolve(HomeRouterProtocol.self)!)
            routers.append(assembler.resolver.resolve(ScheduleRouterProtocol.self)!)
            routers.append(assembler.resolver.resolve(RequestsRouterProtocol.self)!)
            routers.append(assembler.resolver.resolve(ProfileRouterProtocol.self)!)
            routers.append(assembler.resolver.resolve(SettingsRouterProtocol.self)!)
        } else {
            routers.append(assembler.resolver.resolve(HomeRouterProtocol.self)!)
            routers.append(assembler.resolver.resolve(MastersRouterProtocol.self)!)
            routers.append(assembler.resolver.resolve(SearchRouterProtocol.self)!)
            routers.append(assembler.resolver.resolve(ProfileRouterProtocol.self)!)
            routers.append(assembler.resolver.resolve(SettingsRouterProtocol.self)!)
        }
        self.routers = routers
        
        var controllers = [UIViewController]()
        for router in routers {
            controllers.append(router.rootController)
        }
        tabBarController.setViewControllers(controllers, animated: false)
        
        return tabBarController
    }
    
}
