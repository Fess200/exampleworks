//
//  StartRouter.swift
//  Masters
//
//  Created by Andrew on 05.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class StartRouter: NSObject, StartRouterProtocol, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    private var completeSelectPhoto: ((_ image:UIImage?)->())?
    
    private var navigationController: UINavigationController!
    
    private let menuRouter: MenuRouterProtocol
    
    private let profileService: ProfileServiceProtocol
    
    let window: UIWindow
    
    init(menuRouter: MenuRouterProtocol, profileService: ProfileServiceProtocol, window: UIWindow) {
        self.menuRouter = menuRouter
        self.window = window
        self.profileService = profileService
    }
    
    var rootController: UIViewController {
        
//        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCRegistration") as! VCRegistration
//        controller.viewModel = RegistrationViewModel(router: self)
//        navigationController = UINavigationController(rootViewController: controller)
//        return navigationController
        
        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCLogin") as! VCLogin
        controller.viewModel = LoginViewModel(router: self)
        navigationController = UINavigationController(rootViewController: controller)
        return navigationController
    }
    
    func showEnterSMS() {
        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCSendSMS") as! VCSendSMS
        controller.presenter = SendSMSPresenter(view: controller, router: self)
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showRegistration(phone: String) {
        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCRegistration") as! VCRegistration
        controller.viewModel = RegistrationViewModel(router: self, phone: phone)
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showAddService(viewModel:AddServiceViewModel) {
        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCAddService") as! VCAddService
        controller.viewModel = viewModel
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showEditPeriodInterval(viewModel:PeriodIntervalViewModel) {
        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCPeriodInterval") as! VCPeriodInterval
        controller.viewModel = viewModel
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showSelectPeriodInterval(viewModel:SelectPeriodIntervalViewModel) {
        let controller = UIStoryboard.storyBoardAlerts().instantiateViewController(withIdentifier: "VCSelectTime") as! VCSelectTime
        controller.viewModel = viewModel
        controller.modalPresentationStyle = .overFullScreen
        navigationController.present(controller, animated: true, completion: nil)
    }
    
    func showSelectSeviceCategory(viewModel:SelectServiceCategoryViewModel) {
        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCSelectServiceCategory") as! VCSelectServiceCategory
        controller.viewModel = viewModel
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showSelectSevice(viewModel:SelectServiceViewModel) {
        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCSelectService") as! VCSelectService
        controller.viewModel = viewModel
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showSelectDuration(viewModel:SelectDurationViewModel) {
        let controller = UIStoryboard.storyBoardAlerts().instantiateViewController(withIdentifier: "VCSelectTime") as! VCSelectTime
        controller.viewModel = viewModel
        controller.modalPresentationStyle = .overFullScreen
        navigationController.present(controller, animated: true, completion: nil)
    }
    
    func showDetailPreview(viewModel:DetailPreviewExampleWorkViewModel) {
        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCDetailPreviewExampleWork") as! VCDetailPreviewExampleWork
        controller.viewModel = viewModel
        navigationController.present(controller, animated: true, completion: nil)
    }
    
    func showEditDaysWeekend(viewModel:WeekendDaysViewModel) {
        let controller = UIStoryboard.storyBoardStartScreen().instantiateViewController(withIdentifier: "VCWeekendDays") as! VCWeekendDays
        controller.viewModel = viewModel
        navigationController.pushViewController(controller, animated: true)
    }
    
    func showSelectData(viewModel:SelectDataViewModel) {
        let controller = UIStoryboard.storyBoardCommon().instantiateViewController(withIdentifier: "SelectDataVC") as! SelectDataVC
        controller.viewModel = viewModel
        controller.modalPresentationStyle = .overFullScreen
        navigationController.present(controller, animated: true, completion: nil)
    }
    
    func dismiss() {
        navigationController.presentedViewController?.dismiss(animated: true, completion: nil)
    }
    
    func goBack() {
        navigationController.popViewController(animated: true)
    }
    
    func showAlertNeedReg(complete:@escaping ((_ success:Bool)->())) {
        let alertController = UIAlertController(title: "Внимание", message: "Пользователь с таким номером не зарегистрирован, вы хотите зарегистрироваться?", preferredStyle: .alert)
        let actionOK = UIAlertAction(title: "ОК", style: .default) { (_) in
            complete(true)
        }
        alertController.addAction(actionOK)
        let actionCancel = UIAlertAction(title: "Отмена", style: .cancel) { (_) in
            complete(false)
        }
        alertController.addAction(actionCancel)
        navigationController.present(alertController, animated: true, completion: nil)
    }
    
    func showSelectPhoto(complete:@escaping (_ image:UIImage?)->()) {
        
        self.completeSelectPhoto = complete
        
        let alertController = UIAlertController(title: "Выбрать аватар", message: nil, preferredStyle: UI_USER_INTERFACE_IDIOM() == .pad ? .alert : .actionSheet)
        
        alertController.addAction(UIAlertAction(title: "Галерея", style: .default, handler: { (alert:UIAlertAction) in
            
            let imagePickerController = UIImagePickerController()
            
            imagePickerController.sourceType = .photoLibrary
            
            imagePickerController.delegate = self
            
            self.navigationController.present(imagePickerController, animated: true, completion: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: "Сделать снимок", style: .default, handler: { (alert:UIAlertAction) in
            
            let imagePickerController = UIImagePickerController()
            
            imagePickerController.sourceType = .camera
            
            imagePickerController.delegate = self
            
            self.navigationController.present(imagePickerController, animated: true, completion: nil)

        }))
        
        alertController.addAction(UIAlertAction(title: "Отмена", style: .cancel, handler: { (alert:UIAlertAction) in
            self.completeSelectPhoto?(nil)
            self.completeSelectPhoto = nil
        }))
        
        navigationController.present(alertController, animated: true, completion: nil)
    }
    
    // MARK: - UIImagePickerControllerDelegate
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        picker.dismiss(animated:true, completion: nil)
        
        self.completeSelectPhoto?(image)
        self.completeSelectPhoto = nil
    }
    
    func changeToTabBarController() {
        if let _ = profileService.getUser() {
            self.navigationController.setNavigationBarHidden(true, animated: true)
            CATransaction.begin()
            CATransaction.setCompletionBlock({ 
                
            })
            self.navigationController.pushViewController(menuRouter.rootController, animated: true)
            CATransaction.commit()
        }
    }
    
}
