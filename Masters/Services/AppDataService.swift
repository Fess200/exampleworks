//
//  AppDataService.swift
//  Masters
//
//  Created by Andrew on 31.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit

class AppDataService: NSObject, AppDataServiceProtocol {
    
    private let network: AppDataNetworkProtocol
    private(set) var volcabuary: Volcabuary?
    
    init(network: AppDataNetworkProtocol) {
        self.network = network
        
        super.init()
    }
    
    func updateVolcabuary(complete: @escaping ((_ volcabuary: Volcabuary?)->())) {
        self.network.getVolcabuary(complete: { (volcabuary, error) in
            self.volcabuary = volcabuary
            complete(self.volcabuary)
        })
    }
    
    func getCategories(complete: @escaping ((_ categories: [ServiceCategory]?)->())) {
        
        guard let categories = self.volcabuary?.serviceCategories else {
            self.updateVolcabuary(complete: { (volcabuary:Volcabuary?) in
                complete(volcabuary?.serviceCategories)
            })
            return
        }
        
        complete(categories)
    }
    
    func getServices(categoryId: Int, complete: @escaping ((_ services: [Service]?)->())) {
        
        let blockGetServices = {
            
            if let serviceCategory = self.volcabuary?.serviceCategories.filter({ (category:ServiceCategory) -> Bool in
                return category.id == categoryId
            }).first {
                complete(serviceCategory.services)
            }
        }
        
        guard let _ = self.volcabuary?.serviceCategories else {
            self.updateVolcabuary(complete: { (volcabuary:Volcabuary?) in
                blockGetServices()
            })
            return
        }
        
        blockGetServices()
    }
    
    func getRegions(complete: @escaping ((_ services: [Region])->())) {
        let blockGetRegions = {
            if let regions = self.volcabuary?.regions {
                complete(regions)
            }
        }
        
        guard let _ = self.volcabuary?.regions else {
            self.updateVolcabuary(complete: { (volcabuary:Volcabuary?) in
                blockGetRegions()
            })
            return
        }
        
        blockGetRegions()

    }
    
}
