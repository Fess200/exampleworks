//
//  ProfileService.swift
//  Masters
//
//  Created by Andrew on 05.01.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

import UIKit
import RealmSwift

class ProfileService: NSObject, ProfileServiceProtocol {
    
    private let network:ProfileNetworkProtocol
    
    init(network:ProfileNetworkProtocol) {
        self.network = network
    }
    
    func getUser() -> User? {
        let realm = try! Realm()
        return realm.objects(User.self).first
    }
    
    func isMaster() -> Bool {
        if let user = getUser() {
            return user.isMaster
        }
        
        return false
    }
    
    func token() -> String? {
        if let user = getUser() {
            return user.token
        }
        
        return nil
    }
    
    func logout() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }
    }
    
    func sendCodeSMS(phone:String, complete: @escaping ((_ success:Bool, _ error:NetworkError?)->())) -> URLSessionTask? {
        return network.sendCodeSMS(phone: phone, complete: complete)
    }
    
    func registerUser(name:String, nick:String, phone:String, address:String, avatar: UIImage?, isMaster: Bool, about:String?, holidays: [DayWeekend]?, breaks: [PeriodInterval]?, examplesWorks: [UIImage]?, complete: @escaping ((_ success:Bool, _ error:NetworkError?)->())) {
        network.registerUser(name: name, nick: nick, phone: phone, address: address, avatar: avatar, isMaster: isMaster, about: about, holidays: holidays, breaks: breaks, examplesWorks: examplesWorks) { (user:User?, error:NetworkError?) in
            if let user = user {
                let realm = try! Realm()
                try! realm.write {
                    realm.add(user)
                }
                complete(true, nil)
            }
        }
    }
}
