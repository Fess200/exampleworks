//
//  Utils.swift
//  Masters
//
//  Created by Andrew on 25.03.17.
//  Copyright © 2017 Andrew. All rights reserved.
//

func correct(phone:String) -> String {
    var phone = phone
    phone = phone.replacingOccurrences(of: "(", with: "")
    phone = phone.replacingOccurrences(of: ")", with: "")
    phone = phone.replacingOccurrences(of: "-", with: "")
    phone = phone.replacingOccurrences(of: "(", with: "")
    phone = phone.replacingOccurrences(of: "+7", with: "")
    return phone
}
